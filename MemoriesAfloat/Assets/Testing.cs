using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{

    [SerializeField]
    private GestioDeTemps m_GestioDeTemps;

    [SerializeField] private GameObject prefab;

    [SerializeField] List<ObjecteScriptable>aiuda;
    public List<PeixScriptable> peixScripts;
    public List<CraftScriptable> craftScripts;
    // Update is called once per frame
    void Update()
    {
        /*
        if(Input.GetKeyUp(KeyCode.L))
        {
            m_GestioDeTemps.CanviDia();
        }
        */

        /*
        if(Input.GetKeyUp(KeyCode.L))
            prefab.SetActive(true);
        */
        if (Input.GetKeyUp(KeyCode.L))
        {
            /*
            Debug.Log(aiuda);

            if (aiuda is PeixScriptable peix)
            {
                
                Debug.Log("Soy pez"+peix);
            }

            if(aiuda is CraftScriptable craft)
            {
                Debug.Log("Soy esquer"+craft);
            }
            */

            foreach (ObjecteScriptable obj in aiuda)
            {
                if(obj is PeixScriptable)
                {
                    peixScripts.Add((PeixScriptable)obj);
                }

                if(obj is CraftScriptable)
                {
                    craftScripts.Add((CraftScriptable)obj);
                }
            }
        }
    }
}
