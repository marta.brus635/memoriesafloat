using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class TestingManager : MonoBehaviour
{
    private static TestingManager m_Instance;

    [SerializeField] private GameEvent m_MiniJocCanya;
    [SerializeField] private GameEvent m_PeixPerdut;
    private Coroutine m_PreMiniJocCanya;
    private bool isCaught;
    [SerializeField] private GameObject AvisoProvisional;

    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;

    [SerializeField] private CanyaScriptable m_CanyaScriptable;

    public static TestingManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        m_PlayerInputActions = new PlayerInputSystem();

        DontDestroyOnLoad(gameObject);
        //SceneManager.sceneLoaded += OnSceneLoaded;

    }

    public void PreMinijoc()
    {
        m_PreMiniJocCanya = StartCoroutine(PreMiniJocCanya());
        m_PlayerInputActions.PreMJCanya.Enable();
        isCaught = false;
    }

    IEnumerator PreMiniJocCanya()
    {
        while (!isCaught)
        {
            int number = Random.Range(m_CanyaScriptable.MusiquitaDeAscensor, 11);
            Debug.Log("GACHAPON:" + number);
            if (number == 10)
            {
                AvisoProvisional.SetActive(true);
                int contador = 1;
                m_PlayerInputActions.PreMJCanya.Pescar.performed += MJCanyaStart;

                while (contador <= m_CanyaScriptable.TempsDeReaccio)
                {
                    Debug.Log("CONTADOR:" + contador);
                    yield return new WaitForSeconds(1);
                    contador++;
                }

                PeixHaEscapat();
            }
            yield return new WaitForSeconds(2f);
        }

    }

    private void SalirPreMJCanya()
    {
        AvisoProvisional.SetActive(false);
        m_PlayerInputActions.PreMJCanya.Pescar.performed -= MJCanyaStart;
        m_PlayerInputActions.PreMJCanya.Disable();
        StopCoroutine(m_PreMiniJocCanya);
    }

    private void PeixHaEscapat()
    {
        SalirPreMJCanya();
        m_PeixPerdut.Raise();
    }

    private void MJCanyaStart(InputAction.CallbackContext context)
    {
        SalirPreMJCanya();
        m_MiniJocCanya.Raise();

    }

}
