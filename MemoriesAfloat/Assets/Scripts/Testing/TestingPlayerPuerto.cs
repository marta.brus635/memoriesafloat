using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestingPlayerPuerto : MonoBehaviour
{
    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;
    private Rigidbody2D m_Rigidbody;
    [SerializeField] private float m_Speed;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();

        m_PlayerInputActions = new PlayerInputSystem();
        m_PlayerInputActions.Port.Enable();
        m_PlayerInputActions.Port.Moviment.performed += Moviment;
        m_PlayerInputActions.Port.Moviment.canceled += Stop;
    }

    private void Moviment(InputAction.CallbackContext context)
    {
        Vector2 inputVector = m_PlayerInputActions.Port.Moviment.ReadValue<Vector2>();
        m_Rigidbody.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed;

    }

    private void Stop(InputAction.CallbackContext context)
    {
        m_Rigidbody.velocity = Vector2.zero;

    }


}
