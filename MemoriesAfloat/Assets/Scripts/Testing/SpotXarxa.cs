using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotXarxa : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_MJXEnd;
    private SpotsManager m_papa;

    private List<PeixScriptable> m_peixosCapturats;

    [SerializeField]
    private PeixScriptable peix;
    [SerializeField]
    private PeixScriptable peixExtra;

    private void Start()
    {
        m_papa = GetComponentInParent<SpotsManager>();
    }
    public void GestionPeces(int peces, int pecesExtra)
    {
        Debug.Log("Gestionando pececitos");

        if (peces > 0)
        {
            for (int i = 1; i <= peces; i++)
            {
                m_papa.PlayerScript.CapturaPeix(peix);
            }
        }
        if (pecesExtra > 0)
        {
            for (int i = 1; i <= pecesExtra; i++)
            {
                m_papa.PlayerScript.CapturaPeix(peixExtra);
            }
        }

        m_MJXEnd.Raise();
    }
}
