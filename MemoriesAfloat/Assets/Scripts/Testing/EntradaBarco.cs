using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntradaBarco : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Entrando al barco");
            collision.gameObject.SetActive(false);
            GameManager.Instance.EntradaVaixell();
        }
    }
}
