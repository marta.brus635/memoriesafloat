using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotPesca : MonoBehaviour
{
    private SpotsManager m_papa;

    [SerializeField]
    private PeixScriptable m_peixDelSpot;
    public PeixScriptable peixSpot
    {
        get { return m_peixDelSpot; }
    }
    private void Start()
    {
        m_papa = GetComponentInParent<SpotsManager>();
    }
    public void GetionPecesCanya(PeixScriptable peix)
    {
        if(peix!=null)
            m_papa.PlayerScript.CapturaPeix(peix);

    }
}
