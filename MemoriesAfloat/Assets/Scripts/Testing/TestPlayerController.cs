using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestPlayerController : MonoBehaviour
{
    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;
    private SpotPesca m_PescaSpot;
    private SpotXarxa m_XarxaSpot;
    private Rigidbody2D m_Rigidbody;
    [SerializeField] private float m_Speed;
    [SerializeField] private GameEvent MinijocCanya;
    [SerializeField] private GameEvent MinijocXarxa;

    private GameObject miniJocXarxa;

    private void Awake()
    {
        //Components
        m_Rigidbody = GetComponent<Rigidbody2D>();

        //Minijocs
        miniJocXarxa = GameObject.Find("MiniJoc Xarxa Hanlder");

        //Input System
        m_PlayerInputActions = new PlayerInputSystem();
        m_PlayerInputActions.Vaixell.Enable();
        m_PlayerInputActions.Vaixell.Pescar.performed += Pescar;
        m_PlayerInputActions.Vaixell.Moviment.performed += Moviment;
        m_PlayerInputActions.Vaixell.Moviment.canceled += Stop;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = collision.gameObject.GetComponent<SpotPesca>();
        }

        if (collision.gameObject.tag == "Xarxa")
        {
            m_XarxaSpot = collision.gameObject.GetComponent<SpotXarxa>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = null;
        }

        if (collision.gameObject.tag == "Xarxa")
        {
            m_XarxaSpot = null;
        }
    }

    private void Pescar(InputAction.CallbackContext context)
    {
        if (m_PescaSpot != null)
        {
            //Debug.Log("Minijoc de canya");
            m_PlayerInputActions.Vaixell.Disable();
            TestingManager.Instance.PreMinijoc();

        }
        else if (m_XarxaSpot != null)
        {
            Debug.Log("Minijoc de Xarxa");
            m_PlayerInputActions.Vaixell.Disable();
            miniJocXarxa.SetActive(true);
            MinijocXarxa.Raise();

        }
    }


    private void Moviment(InputAction.CallbackContext context)
    {
        Vector2 inputVector = m_PlayerInputActions.Vaixell.Moviment.ReadValue<Vector2>();
        m_Rigidbody.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed;
    }

    private void Stop(InputAction.CallbackContext context)
    {
        m_Rigidbody.velocity = Vector3.zero;
    }

    public void Tornada()
    {
        m_PlayerInputActions.Vaixell.Enable();
        if (m_PescaSpot != null)
        {
            Destroy(m_PescaSpot.gameObject);
        }
        else if (m_XarxaSpot != null)
        {
            Destroy(m_XarxaSpot.gameObject);
        }
    }
}
