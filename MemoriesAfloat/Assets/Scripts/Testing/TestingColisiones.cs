using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingColisiones : MonoBehaviour
{
    private Rigidbody2D m_RigidBody;
    [SerializeField] private float m_Speed;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            m_RigidBody.velocity = -Vector2.right * Time.deltaTime * m_Speed;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            m_RigidBody.velocity = Vector2.right * Time.deltaTime * m_Speed;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            m_RigidBody.velocity = Vector2.up * Time.deltaTime * m_Speed;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            m_RigidBody.velocity = -Vector2.up * Time.deltaTime * m_Speed;
        }

    }
}
