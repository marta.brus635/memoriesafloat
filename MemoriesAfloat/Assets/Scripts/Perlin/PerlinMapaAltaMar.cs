using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor.ShaderGraph.Drawing;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

public class PerlinMapaAltaMar : MonoBehaviour
{
    //[SerializeField] private Tile[] tiles;
    [SerializeField] private AnimatedTile[] tiles;
    [SerializeField] private Tilemap tilemap;

    [SerializeField] private int m_Width;
    [SerializeField] private int m_Height;

    [SerializeField]
    private GestioDeTemps m_GestioDeTemps;

    //Scale
    [SerializeField]
    private float m_Scale = 1000f;
    //Scale
    [SerializeField]
    private float m_Frequency = 10f;

    //octaves
    private const int MAX_OCTAVES = 8;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_Octaves = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_Lacunarity = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_Persistence = 0.5f;
    [Tooltip("Do the octaves carve the terrain?")]
    [SerializeField]
    private bool m_Carve = true;


    [Header("Aparicio Spots")]
    [Tooltip("0:Canya 1:Xarxa 2:Busseig")]
    [SerializeField] private GameObject[] spots;
    [SerializeField] private List<GameObject> spotsBeforeCleaning;
    [SerializeField] private SpawnedSpots spawnedSpots;
    [SerializeField] private int frecuency;

    private void Awake()
    {
        GenerarMapa();

    }

    private void GenerarMapa()
    {
        Reset();
        GeneratePerlinMap();
        NetejarSpots();

    }

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.L))
        {
            GenerarMapa();
        }
    }
    //private void Update()
    //{
    //    if (Input.GetKeyUp(KeyCode.Space))
    //    {
    //        Reset();
    //        GeneratePerlinMap();
    //        NetejarSpots();
    //    }
    //}


    private void GeneratePerlinMap()
    {

        for (int x = 0; x < m_Width; x++)
        {
            for (int y = 0; y < m_Height; y++)
            {
                //per cada casella comprovem soroll perlin donats els par�metres
                //les coordenades x i y que buscarem venen despla�ades per l'offset
                //l'escala ens determina quan ens movem per unitat
                //la freq�encia ens determina com de continuus s�n els passos que fem
                float xCoord = m_GestioDeTemps.PerlinSeed + (x / m_Scale) * m_Frequency;
                float yCoord = m_GestioDeTemps.PerlinSeed + (y / m_Scale) * m_Frequency;
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                //Valor base
                //if (m_Verbose) Debug.Log(string.Format("Base: [{0},{1}] = {2}", x, y, sample));
                //colors[0][x + y * m_Width] = m_Gradient.Evaluate(sample);
                //tilemap.SetTile(new Vector3Int(x, y), tiles[0]);

                //Acte seguit calculem les octaves
                for (int octave = 1; octave <= m_Octaves; octave++)
                {
                    //La Lacunarity afecta a la freq�encia de cada subseq�ent octava. El limitem a [2,3] de forma
                    //que cada nou valor sigui x2 o x3 de la freq�encia anterior (doble o triple de soroll)
                    float newFreq = m_Frequency * m_Lacunarity * octave;
                    float xOctaveCoord = m_GestioDeTemps.PerlinSeed + (x / m_Scale) * newFreq;
                    float yOctaveCoord = m_GestioDeTemps.PerlinSeed + (y / m_Scale) * newFreq;

                    //valor base de l'octava
                    float octaveSample = Mathf.PerlinNoise(xOctaveCoord, yOctaveCoord);
                    //colors[octave*2][x + y * m_Width] = m_Gradient.Evaluate(octaveSample);



                    //La Persistence afecta a l'amplitud de cada subseq�ent octava. El limitem a [0.1, 0.9] de forma
                    //que cada nou valor afecti menys al resultat final.
                    //Si m_Carve est� actiu ->
                    //addicionalment, farem que el soroll en comptes de ser un valor base [0,1] sigui [-0.5f,0.5f]
                    //i aix� pugui sumar o restar al valor inicial
                    octaveSample = (octaveSample - (m_Carve ? .5f : 0)) * (m_Persistence / octave);

                    //acumulaci� del soroll amb les octaves i base anteriors
                    //if (m_Verbose) Debug.Log(string.Format("Octave {3}: [{0},{1}] = {2}", x, y, sample, octave));
                    sample += octaveSample;
                    //colors[octave * 2 + 1][x + y * m_Width] = m_Gradient.Evaluate(sample);

                    //Debug.Log(sample);
                }

                //if (m_Verbose) Debug.Log(string.Format("[{0},{1}] = {2}", x, y, sample));

                //i utilitzem el soroll com a factor per a determinar l'al�ada final del terreny
                //heights[x,y] = sample;
                //colors[1][x + y * m_Width] = m_Gradient.Evaluate(sample);

                int gachapon = Random.Range(0, 100);

                if (sample > 0.6f)
                {
                    tilemap.SetTile(new Vector3Int(x, y), tiles[1]);
                    if (gachapon < frecuency /*&& spawnedSpots.spawnedSpots.Count == 0*/)
                        GenerateSpots(spots[0], new Vector3Int(x, y, 0));

                }
                else if (sample > 0.4f)
                {
                    tilemap.SetTile(new Vector3Int(x, y), tiles[2]);
                    if (gachapon < frecuency /*&& spawnedSpots.spawnedSpots.Count == 0*/)
                        GenerateSpots(spots[1], new Vector3Int(x, y, 0));
                }
                else
                {
                    tilemap.SetTile(new Vector3Int(x, y), tiles[0]);
                    if (gachapon < frecuency /*&& spawnedSpots.spawnedSpots.Count == 0*/)
                        GenerateSpots(spots[2], new Vector3Int(x, y, 0));
                }

            }

        }


    }


    private void GenerateSpots(GameObject spot, Vector3Int position)
    {
        Vector3 spawnPoint = tilemap.GetCellCenterWorld(position);
        GameObject go = Instantiate(spot, transform);
        go.transform.position = spawnPoint;
        spotsBeforeCleaning.Add(go);

    }

    private void NetejarSpots()
    {
        for (int x = 0; x < m_Width; x++)
        {
            for (int y = 0; y < m_Height; y++)
            {
                foreach (GameObject go in spotsBeforeCleaning)
                {
                    if (go.transform.position == tilemap.GetCellCenterWorld(new Vector3Int(x, y, 0)))
                    {
                        LimpiarVecinos(go);
                    }
                }
            }
        }

        //FindPlayableSpots();
    }

    private void FindPlayableSpots()
    {
        /*
        foreach (GameObject spot in GameObject.FindGameObjectsWithTag("Canya"))
            spawnedSpots.spawnedSpots.Add(spot);

        foreach (GameObject spot in GameObject.FindGameObjectsWithTag("Xarxa"))
            spawnedSpots.spawnedSpots.Add(spot);

        foreach (GameObject spot in GameObject.FindGameObjectsWithTag("Busseig"))
            spawnedSpots.spawnedSpots.Add(spot);
        */
        /*
        for(int i = 1; i < transform.childCount; i++)
        {
            spawnedSpots.spawnedSpots.Add(transform.GetChild(i).gameObject);
        }
        */
    }

    private void LimpiarVecinos(GameObject spot)
    {
        Vector3 position = spot.transform.position;

        foreach (GameObject go in spotsBeforeCleaning)
        {
            if (go.transform.position == new Vector3(position.x - 1, position.y, 0)
                || go.transform.position == new Vector3(position.x + 1, position.y, 0)
                || go.transform.position == new Vector3(position.x, position.y - 1, 0)
                || go.transform.position == new Vector3(position.x, position.y + 1, 0)
                || go.transform.position == new Vector3(position.x - 1, position.y - 1, 0)
                || go.transform.position == new Vector3(position.x + 1, position.y + 1, 0)
                || go.transform.position == new Vector3(position.x + 1, position.y - 1, 0)
                || go.transform.position == new Vector3(position.x - 1, position.y + 1, 0))
            {
                go.tag = "Untagged";
                Destroy(go);

            }
        }
    }

    private void Reset()
    {
        for (int x = 0; x < m_Width; x++)
        {
            for (int y = 0; y < m_Height; y++)
            {
                tilemap.SetTile(new Vector3Int(x, y), null);
            }
        }

        foreach (GameObject go in spotsBeforeCleaning)
        {
            go.tag = "Untagged";
            Destroy(go);
        }

        spotsBeforeCleaning.Clear();
        //spawnedSpots.spawnedSpots.Clear();
    }
}
