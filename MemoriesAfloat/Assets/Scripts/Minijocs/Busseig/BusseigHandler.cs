using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class BusseigHandler : MonoBehaviour
{
    // - Canvas -
    private Image m_medidorOX;
    private float m_unitatOX;

    private GameObject m_go;

    private PlayerControllerBus m_bus;

    private Coroutine corComptadorBombona;
    private bool m_hasOxy = true;
    //public bool HasOxy { get { return m_hasOxy; } set { m_hasOxy = value; } }

    [SerializeField] private BusseigScriptable m_busseigScript;

    private GameObject m_mySpot;

    [SerializeField] GameEvent MJBusseigEnd;

    private GameObject m_escenari;
    private GameObject[] m_llistaEscenaris = new GameObject[3];
    private GameObject m_gridActual;
    [SerializeField] private GameObject[] m_llistaGrids = new GameObject[3];
    private Transform[] m_spawners = new Transform[3];
    //private void Start()
    //{
    //    StartMinijoc(null);
    //}
    public void StartMinijoc(GameObject spot)
    {
        m_mySpot = spot;

        m_bus = GameObject.Find("PlayerBusseig").GetComponent<PlayerControllerBus>();
        m_medidorOX = GameObject.Find("medidorOX").GetComponentInChildren<Image>();
        m_medidorOX.fillAmount = 1.0f;
        m_unitatOX = (0.1f / m_busseigScript.tempsBombonaAct) * 10;

        //Seleccionar els escenaris
        m_llistaEscenaris = GameObject.FindGameObjectsWithTag("Escenari");
        //m_escenari = GameObject.Find("Escenaris").transform.GetChild(Random.Range(0, 4)).gameObject;
        int gachapon = Random.Range(0, 3);
        m_escenari = m_llistaEscenaris[gachapon];
        m_gridActual = m_llistaGrids[gachapon];
        foreach (GameObject e in m_llistaEscenaris)
        {
            if (e != m_escenari) { e.SetActive(false); }
        }
        foreach (GameObject g in m_llistaGrids)
        {
            if (g != m_gridActual) { g.SetActive(false); }
        }

        for (int i = 0; i < 3; i++)
        {
            m_spawners[i] = m_escenari.transform.GetChild(i).transform;
        }

        SpawnMoluscs();
    }

    private void SpawnMoluscs()
    {
        foreach (GameObject m in m_busseigScript.goMoluscs)
        {
            m_go = Instantiate(m, gameObject.transform);
            m_go.transform.position = gachaponSpawn();
        }
        corComptadorBombona = StartCoroutine(ComptadorBombona());
    }

    private Vector3 gachaponSpawn()
    {
        switch (Random.Range(0, 3))
        {
            case 0: return m_spawners[0].position;
            case 1: return m_spawners[1].position;
            case 2: return m_spawners[2].position;
            default: return m_spawners[0].position;
        }
    }

    IEnumerator ComptadorBombona()
    {
        while (m_hasOxy)
        {
            yield return new WaitForSeconds(1.5f);
            m_busseigScript.tempsBombonaAct--;
            //Debug.Log(string.Format("Temps: {0} / {1}", m_tempsAct, m_tempsMax));
            m_medidorOX.fillAmount = m_medidorOX.fillAmount - m_unitatOX;
            if (m_busseigScript.tempsBombonaAct <= 0)
            {
                m_bus.PlayerScript.numBombonesOX--;
                m_busseigScript.tempsBombonaAct = m_busseigScript.tempsBombonaMax;
                BusseigEnd();
            }
        }
    }

    public void BusseigEnd()
    {
        //Debug.Log("S'ha acabat el Busseig");
        m_hasOxy = false;
        if (corComptadorBombona != null)
            StopCoroutine(corComptadorBombona);

        m_bus.Actions.MinijocBusseig.Disable();

        // GestionarCaptures
        foreach (PeixScriptable m in m_bus.Capturas)
        {
            Debug.Log("Captura: " + m.name + " (" + m.nom + ")");
            m_bus.PlayerScript.CapturaPeix(m);
        }

        MJBusseigEnd.Raise();
    }

    public void poof(GameObject poofed)
    {

        //Debug.Log("CojoCosas");
        poofed.SetActive(false);
    }

}
