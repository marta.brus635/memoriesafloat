using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Unity.VisualScripting;
using Unity.VisualScripting.FullSerializer;
using UnityEditor.Tilemaps;
using UnityEngine;
using Color = UnityEngine.Color;
using Transform = UnityEngine.Transform;

public class MoluscsBehaviour : MonoBehaviour
{
    private BusseigHandler m_papa;
    [SerializeField] private PeixScriptable m_moluscScript;
    public PeixScriptable MoluscsScript { get { return m_moluscScript; } }
    private Rigidbody2D m_rb;

    // - Fugir -
    private bool m_anxiety = false;
    private Vector3 directionToEscape;
    private Collider2D m_player;
    // - Manage Directions -
    private RaycastHit2D m_hit;
    private RaycastHit2D m_hitParet1;
    [SerializeField] private float m_rayDistance;
    [SerializeField] ContactFilter2D m_filerlayerMask;
    private List<RaycastHit2D> m_hitResults = new();
    private Vector3 playerPos;


    // - Comportaments -
    private Coroutine corEstatic;
    private Coroutine corEspantadis;
    private Coroutine corMovil;
    private Coroutine corIsItSafe;
    private bool m_isbehaving = false;
    private int m_rand;


    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_papa = transform.parent.GetComponent<BusseigHandler>();

        switch (m_moluscScript.busseigComportament)
        {
            case comportament.estatic:
                m_isbehaving = true;
                corEstatic = StartCoroutine(BehaviourEstatic());
                break;
            case comportament.espantadis:
                m_isbehaving = true;
                corEspantadis = StartCoroutine(BehaviourEspantadis());
                break;
            case comportament.movil:
                m_isbehaving = true;
                corMovil = StartCoroutine(BehaviourMovil());
                break;
            default:
                m_isbehaving = true;
                corEstatic = StartCoroutine(BehaviourEstatic());
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && m_moluscScript.busseigComportament == comportament.espantadis)
        {
            if (m_anxiety == true && corIsItSafe != null)
            {
                StopCoroutine(corIsItSafe);
            }
            else
            {
                m_anxiety = true;
                m_player = col;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && m_moluscScript.busseigComportament == comportament.espantadis && gameObject.activeSelf)
        {
            corIsItSafe = StartCoroutine(IsItSafe());
        }
    }

    IEnumerator BehaviourEstatic()
    {
        while (m_isbehaving)
        {
            m_rand = Random.Range(1, 6);
            switch (m_rand)
            {
                case 1:
                    m_rb.AddForce(transform.up, ForceMode2D.Impulse);
                    break;
                case 2:
                    m_rb.AddForce(transform.right, ForceMode2D.Impulse);
                    break;
                case 3:
                    m_rb.AddForce(-transform.right, ForceMode2D.Impulse);
                    break;
                case 4:
                    m_rb.AddForce((transform.right / 2 + transform.up / 2), ForceMode2D.Impulse);
                    break;
                case 5:
                    m_rb.AddForce((-transform.right / 2 + transform.up / 2), ForceMode2D.Impulse);
                    break;
            }
            yield return new WaitForSeconds(Random.Range(2, 4));
        }
    }
    IEnumerator BehaviourEspantadis()
    {
        while (m_isbehaving)
        {
            if (!m_anxiety)
            {
                m_rand = Random.Range(1, 9);
                switch (m_rand)
                {
                    case 1:
                        m_rb.AddForce(transform.up, ForceMode2D.Impulse);
                        break;
                    case 2:
                        m_rb.AddForce(transform.right, ForceMode2D.Impulse);
                        break;
                    case 3:
                        m_rb.AddForce(-transform.right, ForceMode2D.Impulse);
                        break;
                    case 4:
                        m_rb.AddForce((transform.right / 2 + transform.up / 2), ForceMode2D.Impulse);
                        break;
                    case 5:
                        m_rb.AddForce(-transform.up, ForceMode2D.Impulse);
                        break;
                    case 6:
                        m_rb.AddForce((-transform.right / 2 + -transform.up / 2), ForceMode2D.Impulse);
                        break;
                    case 7:
                        m_rb.AddForce((transform.right / 2 + -transform.up / 2), ForceMode2D.Impulse);
                        break;
                    case 8:
                        m_rb.AddForce((-transform.right / 2 + transform.up / 2), ForceMode2D.Impulse);
                        break;

                }
                yield return new WaitForSeconds(Random.Range(0.5f, 2));
            }
            else
            {
                playerPos = m_player.transform.position;
                if (RaycastEnDireccio(transform.up))
                {
                    m_hitParet1 = m_hit;    //  Guardo la pared para poder girar luego
                    Debug.DrawRay(transform.position, transform.up, Color.red, 1.5f);
                    CanviarDireccio();
                }
                else
                {
                    directionToEscape = (playerPos - transform.position) * -1;
                }
                transform.up = directionToEscape;
                m_rb.AddForce(transform.up, ForceMode2D.Impulse);

                yield return new WaitForSeconds(0.5f);

            }
        }

        // voy a rotarme la cabeza
        //transform.eulerAngles = Vector3.forward * degrees;
        //transform.rotation = Quaternion.Euler(Vector3.forward * degrees);
        //transform.rotation = Quaternion.LookRotation(Vector3.forward, yAxisDirection);

    }
    IEnumerator BehaviourMovil()
    {
        while (m_isbehaving)
        {
            m_rand = Random.Range(1, 9);
            switch (m_rand)
            {
                case 1:
                    m_rb.AddForce(transform.up, ForceMode2D.Impulse);
                    break;
                case 2:
                    m_rb.AddForce(transform.right, ForceMode2D.Impulse);
                    break;
                case 3:
                    m_rb.AddForce(-transform.right, ForceMode2D.Impulse);
                    break;
                case 4:
                    m_rb.AddForce((transform.right / 2 + transform.up / 2), ForceMode2D.Impulse);
                    break;
                case 5:
                    m_rb.AddForce(-transform.up, ForceMode2D.Impulse);
                    break;
                case 6:
                    m_rb.AddForce((-transform.right / 2 + -transform.up / 2), ForceMode2D.Impulse);
                    break;
                case 7:
                    m_rb.AddForce((transform.right / 2 + -transform.up / 2), ForceMode2D.Impulse);
                    break;
                case 8:
                    m_rb.AddForce((-transform.right / 2 + transform.up / 2), ForceMode2D.Impulse);
                    break;

            }

            yield return new WaitForSeconds(Random.Range(0.5f, 4));
        }
    }

    private void CanviarDireccio()
    {
        m_hitResults?.Clear();
        if (RaycastEnDireccio(transform.right))
            directionToEscape = new Vector3(transform.right.x - m_hit.normal.x + m_hit.normal.x / 2, transform.right.y - m_hit.normal.y + m_hit.normal.y / 2, 0.0f);    //transform.right-
        else if (RaycastEnDireccio(-transform.right))
            directionToEscape = new Vector3(transform.right.x + m_hit.normal.x / 2, transform.right.y + m_hit.normal.y / 2, 0.0f);        //transform.right+
        else        //  Si hi ha dos parets
        {
            if (Random.Range(0, 3) == 1)
                directionToEscape = new Vector3(transform.right.x - m_hit.normal.x + m_hit.normal.x / 2, transform.right.y - m_hit.normal.y + m_hit.normal.y / 2, 0.0f);
            else
                directionToEscape = new Vector3(transform.right.x + m_hit.normal.x / 2, transform.right.y + m_hit.normal.y / 2, 0.0f);
        }
    }

    IEnumerator IsItSafe()
    {
        yield return new WaitForSeconds(2.0f);
        m_anxiety = false;
    }
    private bool RaycastEnDireccio(Vector3 direcio)
    {
        m_hitResults?.Clear();
        if (Physics2D.Raycast(transform.position, direcio, m_filerlayerMask, m_hitResults, m_rayDistance) > 0)     // si s'ha trobat amb algo
        {
            foreach (RaycastHit2D hit in m_hitResults)
            {
                if (hit.collider.gameObject.layer == 8) // 8 = layer de las paredes
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void OnDisable()
    {
        m_isbehaving = false;
        m_anxiety = false;

        if (corEspantadis != null)
            StopCoroutine(BehaviourEspantadis());
        if (corEstatic != null)
            StopCoroutine(BehaviourEstatic());
        if (corMovil != null)
            StopCoroutine(BehaviourMovil());
    }
}