using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusseigExit : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameObject.Find("MiniJoc Busseig Handler").GetComponent<BusseigHandler>().BusseigEnd();
        }
    }
}
