using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PreCanyaController : MonoBehaviour
{
    [SerializeField] private GameEvent m_MiniJocCanya;
    [SerializeField] private GameEvent m_PreMJCEnd;
    private Coroutine m_PreMiniJocCanya;
    private bool isCaught;
    [SerializeField] private GameObject AvisoProvisional;

    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;

    [SerializeField] private CanyaScriptable m_CanyaScriptable;

    public void PreMinijoc()
    {
        Debug.Log("que no te salpique...");
        m_PlayerInputActions = new PlayerInputSystem();
        m_PlayerInputActions.PreMJCanya.Enable();
        isCaught = false;
        m_PreMiniJocCanya = StartCoroutine(PreMiniJocCanya());
        
    }

    IEnumerator PreMiniJocCanya()
    {
        while (!isCaught)
        {
            int number = Random.Range(m_CanyaScriptable.MusiquitaDeAscensor, 11);
            if (number == 10)
            {
                AvisoProvisional.SetActive(true);
                int contador = 1;
                m_PlayerInputActions.PreMJCanya.Pescar.performed += MJCanyaStart;

                while (contador <= m_CanyaScriptable.TempsDeReaccio)
                {
                    yield return new WaitForSeconds(1f);
                    contador++;
                }

                PeixHaEscapat();
            }
            yield return new WaitForSeconds(1.5f);
        }

    }
    private void SalirPreMJCanya()
    {
        AvisoProvisional.SetActive(false);
        m_PlayerInputActions.PreMJCanya.Pescar.performed -= MJCanyaStart;
        m_PlayerInputActions.PreMJCanya.Disable();
        isCaught = true;
        if(m_PreMiniJocCanya != null)
            StopCoroutine(m_PreMiniJocCanya);
        m_PreMiniJocCanya = null;
    }

    private void PeixHaEscapat()
    {
        SalirPreMJCanya();
        m_PreMJCEnd.Raise();
    }

    private void MJCanyaStart(InputAction.CallbackContext context)
    {
        SalirPreMJCanya();
        m_MiniJocCanya.Raise();
    }
}
