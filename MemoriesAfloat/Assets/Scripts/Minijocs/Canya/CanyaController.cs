using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanyaController : MonoBehaviour
{
    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;

    private Rigidbody2D m_Rigidbody;
    [SerializeField] private Image m_Comptador;

    private Coroutine m_PosicioPlayer; //si no es corrutina y lo hago con un while crashea.
    private Coroutine m_PujarComptador;
    private Coroutine m_BaixarComptador;

    [SerializeField] private CanyaScriptable m_CanyaScriptable;
    [SerializeField] private GameEvent m_MJCEnd;

    [SerializeField] private Transform WP1;
    [SerializeField] private Transform WP2;

    private bool m_end = false;

    // Spots Stuff
    private SpotPesca m_mySpot;

    private void Awake()
    {
        m_PlayerInputActions = new PlayerInputSystem();
        m_Rigidbody = transform.GetComponent<Rigidbody2D>();

    }

    public void StartingGame()
    {
        m_PlayerInputActions.MinijocCanya.Enable();
        m_PlayerInputActions.MinijocCanya.Moviment.started += Moviment;
        m_PlayerInputActions.MinijocCanya.Moviment.canceled += Stop;
        m_Comptador.fillAmount = 0;
        m_end = false;

        /*
        Debug.Log("0 " + GameObject.FindObjectOfType<PlayerControllerVaixell>());
        Debug.Log("1 " + GameObject.FindObjectOfType<PlayerControllerVaixell>().MySpot);
        Debug.Log("2 " + GameObject.FindObjectOfType<PlayerControllerVaixell>().MySpot.GetComponent<SpotPesca>());
        */
        switch (SceneManager.GetActiveScene().name)
        {
            case "Puerto":
            case "Llac":
                m_mySpot = GameObject.FindObjectOfType<PlayerControllerPuerto>().MySpot.GetComponent<SpotPesca>();
                break;
            case "AltaMar":
                m_mySpot = GameObject.FindObjectOfType<PlayerControllerVaixell>().MySpot.GetComponent<SpotPesca>();
                break;
        }

    }

    private void FixedUpdate()
    {
        m_Rigidbody.velocity = (movimentPlayer ? 1 : -1) * Vector2.right * m_CanyaScriptable.PlayerSpeed;
    }

    bool movimentPlayer = false;
    private void Moviment(InputAction.CallbackContext context)
    {
        movimentPlayer = true;
        //m_PosicioPlayer = StartCoroutine(PosicioPlayer());
    }

    IEnumerator PosicioPlayer()
    {
        while (!m_end)
        {

            //Debug.Log(gameObject.transform.position.x);
            m_Rigidbody.velocity = transform.right * Time.deltaTime * m_CanyaScriptable.PlayerSpeed;
            //m_Rigidbody.AddForce(transform.right * m_VelocitatImpuls, ForceMode2D.Impulse);
            yield return new WaitForEndOfFrame();

        }
    }

    private void Stop(InputAction.CallbackContext context)
    {
        //StopCoroutine(m_PosicioPlayer);
        movimentPlayer = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_PujarComptador = StartCoroutine(PujarComptador());
        if (m_BaixarComptador != null)
            StopCoroutine(m_BaixarComptador);

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!m_end)
        {
            StopCoroutine(m_PujarComptador);
            m_BaixarComptador = StartCoroutine(BaixarComptador());
        }
    }

    IEnumerator PujarComptador()
    {
        while (!m_end)
        {
            m_Comptador.fillAmount += m_CanyaScriptable.FillAmountSpeed;

            if (m_Comptador.fillAmount >= 1)
            {
                Debug.Log("Has pescado el pez!!");
                SortirMinijoc(m_mySpot.peixSpot);
            }


            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator BaixarComptador()
    {
        while (!m_end)
        {
            m_Comptador.fillAmount -= m_CanyaScriptable.EmpyAmountSpeed;
            if (m_Comptador.fillAmount <= 0)
            {
                Debug.Log("Has perdido el pez :(");
                SortirMinijoc(null);
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    private void SortirMinijoc(PeixScriptable peix)
    {
        m_end = true;
        StopAllCoroutines();

        m_PlayerInputActions.MinijocCanya.Moviment.started -= Moviment;
        m_PlayerInputActions.MinijocCanya.Moviment.canceled -= Stop;
        m_PlayerInputActions.MinijocCanya.Disable();
        m_mySpot.GetionPecesCanya(peix);
        m_MJCEnd.Raise();
    }

}
