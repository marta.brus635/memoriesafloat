using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class PeixController : MonoBehaviour
{
    private Rigidbody2D m_Rigidbody;
    private BoxCollider2D m_BoxCollider;
    [SerializeField] private Transform Waypoint1;
    [SerializeField] private Transform Waypoint2;
    private bool isIncreasing;
    private float distance;

    [SerializeField] private CanyaScriptable m_CanyaScriptable;

    private void OnEnable()
    {
        m_Rigidbody = transform.GetChild(0).GetComponent<Rigidbody2D>();
        m_BoxCollider = transform.GetChild(0).GetComponent<BoxCollider2D>();

        isIncreasing = true;
        distance = 0f;
        m_Rigidbody.velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (isIncreasing)
        {
            distance = Mathf.Abs((Waypoint2.transform.position.x - (m_BoxCollider.bounds.size / 2).x) - m_Rigidbody.position.x);
            m_Rigidbody.velocity = transform.right * Time.deltaTime * m_CanyaScriptable.PeixSpeed;
        }
        else
        {
            distance = Mathf.Abs((Waypoint1.transform.position.x + (m_BoxCollider.bounds.size / 2).x) - m_Rigidbody.position.x);
            m_Rigidbody.velocity = -transform.right * Time.deltaTime * m_CanyaScriptable.PeixSpeed;
        }

        //Debug.Log(distance);

        if (distance <= 0.1f)
        {
            //Debug.Log("he llegado");
            isIncreasing = !isIncreasing;
        }   
    }
}
