using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static MatrizGeneratorV2;

public class XarxaHandler : MonoBehaviour
{
    private MatrizGeneratorV2 m_generator;

    private List<MatrizGeneratorV2.Casella> m_caselles = new();
    private List<MatrizGeneratorV2.Casella> m_camiSelec = new();

    private List<MatrizGeneratorV2.Casella> m_extr = new();
    private MatrizGeneratorV2.Casella m_casSelec;
    private List<MatrizGeneratorV2.Casella> m_contigous = new();
    private int col, fila;
    private int c, f;

    private int m_movAct;

    private SpotXarxa m_spot;

    [SerializeField]
    private XarxaScriptable m_scrip;
    public XarxaScriptable Script
    {
        get { return m_scrip; }
    }

    PlayerInputSystem m_playerInput;
    public PlayerInputSystem Actions => m_playerInput;

    // - UI -
    [SerializeField] private TextMeshProUGUI m_txtMov;
    [SerializeField] private TextMeshProUGUI m_txtPeixos;
    private int m_numPeixos;

    private void OnEnable()
    {
        m_playerInput = new PlayerInputSystem();
        m_generator = transform.GetComponentInChildren<MatrizGeneratorV2>();
    }
    public void resetHandler(GameObject spot)
    {
        if (m_generator.Generated)      //  si ya se ha generado lo reseteamos todo
        {
            m_generator.RestartMatrix();

            //Debug.Log("   ---   HANDLER RESTART   ---   ");
            m_camiSelec.Clear();
            m_extr.Clear();
            m_contigous.Clear();


        }
        else
            m_generator.Generate();

        m_caselles = m_generator.Totes;     //     Agafar totes les caselles de la xarxa

        m_playerInput.MinijocXarxa.Enable();
        m_playerInput.MinijocXarxa.Exit.performed += endXarxa;

        m_spot = spot.GetComponent<SpotXarxa>();

        m_numPeixos = 0;
        m_movAct = m_scrip.movMax;
        ActualitzarUI();
    }

    private void ActualitzarUI()
    {
        m_txtMov.text = "" + m_movAct + " / " + m_scrip.movMax;
        m_txtPeixos.text = "" + m_numPeixos;
    }
    public void DiguesCoord(GameObject go)          //  Se llama al hacer click en las casillas
    {

        //Debug.Log("Generator: " + m_generator + " Caselles: " + m_caselles.Count);
        int i = 0;
        foreach (MatrizGeneratorV2.Casella c in m_caselles)
        {
            i++;
            //Debug.Log(string.Format("{3} - pos: ({0}, {1}), go: {2}", c.x, c.y, c.go, i));
        }


        m_casSelec = convertCasillas(go);           //     Agafar el objecte clicat i passar-ho a Casillas
        if (m_casSelec == null) { Debug.Log("PARA QUE NO HAY NADA AQUI: " + go); return; }


        //Debug.Log(String.Format("AlredyCont: {0}, contiguos: {1}, SelecPrimera: {2}", alredyContained(m_casSelec), contiguos(m_casSelec), selecPrimera(m_casSelec)));
        if (!alredyContained(m_casSelec) && contiguos(m_casSelec) && m_movAct > 0 || selecPrimera(m_casSelec) && m_movAct > 0)   // Si la casella no ha sigut seleccionada encara, es contigua o es la primera i esta en els extrems del qudrat
        {
            m_casSelec.go.transform.GetChild(0).gameObject.SetActive(true);
            m_camiSelec.Add(new MatrizGeneratorV2.Casella(m_casSelec.x, m_casSelec.y, m_casSelec.go));
            m_movAct--;

            if (m_casSelec.go.name == "Peix(Clone)" || m_casSelec.go.name == "Peix Extra(Clone)") { m_numPeixos++; }
            ActualitzarUI();

            //Debug.Log(String.Format("mov: {0}/{1}", m_movAct, m_scrip.movMax));
            if (m_movAct == 0)
                endXarxa(new InputAction.CallbackContext());
        }
    }
    private void endXarxa(InputAction.CallbackContext context)
    {
        int numPeix = 0;
        int numPeixExtra = 0;

        foreach (MatrizGeneratorV2.Casella cas in m_camiSelec)
        {
            switch (cas.go.name)
            {
                case "Peix(Clone)":
                    numPeix++;
                    break;
                case "Peix Extra(Clone)":
                    numPeixExtra++;
                    break;
                default:
                    break;
            }
        }

        //Debug.Log(String.Format(" -- Peixos: {0}  PeixosExtra: {1}", numPeix, numPeixExtra));
        m_playerInput.MinijocXarxa.Disable();

        m_spot.GestionPeces(numPeix, numPeixExtra);

        if (m_scrip.percentRed < 1)
            m_scrip.percentRed += 0.1f;

        if (TryGetComponent<UI_Manteniment>(out UI_Manteniment uiManteniment))
            uiManteniment.updateXarxa();
    }

    private bool contiguos(MatrizGeneratorV2.Casella cas)       // Devolver todas las casillas contiguas en cruz
    {
        if (m_camiSelec.Count == 0) return false;               // Si aun no hay casillas seleccionadas no puede haver contiguas

        m_contigous.Clear();                                    // No residuos, gracias

        // cas.x i cas.y = posiciones casilla original
        // c i f = posiciones que va recoriendo el for segun la posicion original (cas)
        c = cas.x;
        f = cas.y;

        // empiezo en laposicion (-1,-1) de la posicion de la casilla actual para mirar todas las contiguas en el 3*3 de alrededor
        for (int i = -1; i < 2; i++)
        {
            col = i + c;
            for (int j = -1; j < 2; j++)
            {
                fila = j + f;
                if (!m_generator.isOutOfRange(col, fila))   //  para el index out of bounds y la casilla original [c,f] 
                {
                    if (col == c || fila == f)              //  para que solo mire las contiguas en cruz e ignore las diagonales
                    {
                        m_contigous.Add(new Casella(col, fila));
                        //Debug.Log(String.Format("Casella Contigua: {0},{1}", col, fila));
                    }
                }
            }
        }

        //  Si la lista de casillas contiguas a la que se ha seleccionado tiene la ultima casilla que se ha seleccionado es que 
        //  es que se esta siguiendo un camino y no cogiendo casillas que no estan pegadas las unas a las otras
        if (compararCasillas(m_contigous, m_camiSelec[m_camiSelec.Count - 1])) return true;
        else return false;
    }

    //  coge el go y busca la casilla entre todas
    //  para sacar la posicion (el transform no me sirve si queremos hacer que 
    //  el tama�o de la red se pueda modificar
    private MatrizGeneratorV2.Casella convertCasillas(GameObject cas)         // Convertir la posicion del GO en posicion de casillas
    {
        foreach (MatrizGeneratorV2.Casella c in m_caselles)
        {
            //Debug.Log(String.Format("pos: ({0}, {1}), go: {2}({3})", c.x, c.y, c.go, cas));
            if (c.go == cas)
            {
                return c;
            }
        }
        return null;
    }

    // Pa no repetir casillas
    private bool alredyContained(MatrizGeneratorV2.Casella cas)
    {
        foreach (MatrizGeneratorV2.Casella c in m_camiSelec) { if (c.go == cas.go) return true; }
        return false;
    }

    //  La primera casilla tiene que ser seleccionada de un extremo del quadraro
    private bool selecPrimera(MatrizGeneratorV2.Casella cas)
    {
        if (m_camiSelec.Count == 0)
        {
            m_extr = m_generator.Extremos();
            if (compararCasillas(m_extr, cas)) return true;
            else return false;
        }
        else
        {
            //Debug.Log("No es la primera (" + m_camiSelec.Count + ")");
            return false;
        }
    }

    // A partir de una lista comprovar que la casilla este en ella
    private bool compararCasillas(List<MatrizGeneratorV2.Casella> lista, MatrizGeneratorV2.Casella cas)
    {
        foreach (MatrizGeneratorV2.Casella c in lista)
        {
            if (c.x == cas.x && c.y == cas.y) return true;
        }
        return false;
    }

}
