using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasellesBehaviour : MonoBehaviour
{
    private XarxaHandler m_handler;
    private Coroutine m_Loading;
    private bool m_loaded;

    private void Awake()
    {
        m_loaded = false;
    }
    void Start()
    {
        m_handler = gameObject.transform.parent.parent.gameObject.GetComponent<XarxaHandler>();
        m_Loading = StartCoroutine(Load());
    }
    private IEnumerator Load()
    {
        m_loaded = false;
        yield return new WaitForSeconds(1.0f);
        m_loaded = true;
        StopCoroutine(m_Loading);
    }
    //private void OnMouseOver()
    //{
    //    if (Input.GetKeyUp(KeyCode.Mouse0))
    //    {
    //        m_handler.DiguesCoord(gameObject);
    //    }
    //}
    private void OnMouseUp()
    {
        if (m_loaded)
        {
            m_handler.DiguesCoord(gameObject);
        }

    }
}
