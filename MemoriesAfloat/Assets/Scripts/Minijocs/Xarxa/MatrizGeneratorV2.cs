using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class MatrizGeneratorV2 : MonoBehaviour
{
    private enum casillas { empty, pec, gujero, pecExtra, chuleto, chuletoStart }
    private casillas[,] m_net;
    public class Casella
    {
        public int x;
        public int y;
        public GameObject go;

        public Casella(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.go = null;
        }

        public Casella(int x, int y, GameObject go)
        {
            this.x = x;
            this.y = y;
            this.go = go;
        }
    }

    private List<GameObject> m_chuletos = new List<GameObject>();
    private casillas[,] m_netChuleto;
    [SerializeField] private bool m_chuleta;

    private GameObject go;
    [SerializeField] private Vector2 m_offset;

    private int m_npecs;
    private int m_Movimentos;               // movimientos del player para coger pecs           
    private List<Casella> m_camino = new List<Casella>();

    // + Listas de posiciones de las casillas
    private List<Casella> m_gujeros = new List<Casella>();
    private List<Casella> m_empties = new List<Casella>();
    private List<Casella> m_pecs = new List<Casella>();
    private List<Casella> m_pecsExtra = new List<Casella>();
    private List<Casella> m_totes = new List<Casella>();
    public List<Casella> Totes
    {
        get { return m_totes; }
    }

    private float m_gujeroChance;
    private bool m_generated = false;
    public bool Generated
    {
        get { return m_generated; }
    }

    private bool m_caminoDone;
    private List<Casella> m_contigous;

    private XarxaScriptable m_scrip;

    private void OnEnable()
    {
        m_scrip = transform.GetComponentInParent<XarxaHandler>().Script;
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.C) && m_chuleta)
            modoChuletos(true);

        if (Input.GetKeyUp(KeyCode.C) && m_chuleta)
            modoChuletos(false);
    }
    public void Generate()
    {
        //if (generated) { RestartMatrix(); } // Resetear la matriz
        // Offset segun si el tama�o de la matriz es inpar o par. Es puramente estetico o para que no se vaya a tomar por saco
        if (m_scrip.size % 2 == 0) { m_offset = new Vector2(m_scrip.size / 2 - 0.5f, m_scrip.size / 2 - 0.5f); }
        else { m_offset = new Vector2(m_scrip.size / 2, m_scrip.size / 2); }

        m_net = new casillas[m_scrip.size, m_scrip.size];
        m_netChuleto = new casillas[m_scrip.size, m_scrip.size];
        //Debug.Log(string.Format("size ---- {0}, {1}", m_net.Length, m_net.GetLength(0)));

        for (int i = 0; i < m_scrip.size; i++)
        {
            for (int j = 0; j < m_scrip.size; j++)
            {
                // Red gujeros
                m_gujeroChance = Random.Range(m_scrip.percentRed, 1.0f);
                if (m_gujeroChance > m_scrip.percentRedRota && m_scrip.percentRed > 0.2f) // si la red esta 0% o al 19% rota mejor no poner gujeros y tal 
                {
                    m_net[i, j] = casillas.gujero;
                    m_gujeros.Add(new Casella(i, j));
                    //m_totes.Add(new Casella(i, j, go));
                }
                else
                {
                    m_net[i, j] = casillas.empty;
                    m_empties.Add(new Casella(i, j));
                    //m_totes.Add(new Casella(i, j, go));
                }
            }
        }
        m_generated = true;
        // Hacer el camino de peces. Empieza en 00. Se podra cambiar
        List<Casella> ext = Extremos();
        Casella casStart = ext[Random.Range(0, ext.Count)];
        //Debug.Log("Comenca a : " + casStart.x + ", " + casStart.y);
        m_camino.Add(new Casella(casStart.x, casStart.y));
        //m_netChuleto[casStart.x, casStart.y] = casillas.chuleto;
        followTheLeader(casStart.x, casStart.y);
        extraPec();
        //emptiesToEmpty();
        Translate(m_net);
        TranslateChuleta(m_netChuleto);

        //Debug.Log("Totes: " + m_totes.Count);
    }
    public List<Casella> Extremos()
    {
        List<Casella> ext = new List<Casella>();

        for (int i = 0; i < m_scrip.size; i++) { ext.Add(new Casella(0, i)); }
        for (int i = 0; i < m_scrip.size; i++) { ext.Add(new Casella(i, 0)); }
        for (int i = 0; i < m_scrip.size; i++) { ext.Add(new Casella(m_scrip.size - 1, i)); }
        for (int i = 0; i < m_scrip.size; i++) { ext.Add(new Casella(i, m_scrip.size - 1)); }

        //return ext[Random.Range(0, ext.Count)];
        return ext;
    }
    private void TranslateChuleta(casillas[,] net)
    {
        for (int i = 0; i < m_scrip.size; i++)
        {
            for (int j = 0; j < m_scrip.size; j++)
            {
                if (net[i, j] == casillas.chuleto)
                {
                    go = Instantiate(m_scrip.prefabChuleto, transform);
                    go.transform.position = new Vector2(i + gameObject.transform.position.x - m_offset.x, j + gameObject.transform.position.y - m_offset.y);
                    m_chuletos.Add(go);
                    go.SetActive(false);
                }
                if (net[i, j] == casillas.chuletoStart)
                {
                    go = Instantiate(m_scrip.prefabChuletoStart, transform);
                    go.transform.position = new Vector2(i + gameObject.transform.position.x - m_offset.x, j + gameObject.transform.position.y - m_offset.y);
                    m_chuletos.Add(go);
                    go.SetActive(false);
                }
            }
        }
    }
    private void modoChuletos(bool mode)
    {
        foreach (GameObject c in m_chuletos)
        {
            c.SetActive(mode);
        }
    }
    private void Translate(casillas[,] net)
    {
        for (int i = 0; i < m_scrip.size; i++)
        {
            for (int j = 0; j < m_scrip.size; j++)
            {
                switch (net[i, j])
                {
                    case casillas.empty:
                        go = Instantiate(m_scrip.prefabEmpty, gameObject.transform);
                        go.transform.position = new Vector2(i + gameObject.transform.position.x - m_offset.x, j + gameObject.transform.position.y - m_offset.y);
                        m_totes.Add(new Casella(i, j, go));
                        break;
                    case casillas.pec:
                        go = Instantiate(m_scrip.prefabPec, gameObject.transform);
                        go.transform.position = new Vector2(i + gameObject.transform.position.x - m_offset.x, j + gameObject.transform.position.y - m_offset.y);
                        m_totes.Add(new Casella(i, j, go));
                        break;
                    case casillas.pecExtra:
                        go = Instantiate(m_scrip.prefabPecExtra, gameObject.transform);
                        go.transform.position = new Vector2(i + gameObject.transform.position.x - m_offset.x, j + gameObject.transform.position.y - m_offset.y);
                        m_totes.Add(new Casella(i, j, go));
                        break;
                    case casillas.gujero:
                        go = Instantiate(m_scrip.prefabGujero, gameObject.transform);
                        go.transform.position = new Vector2(i + gameObject.transform.position.x - m_offset.x, j + gameObject.transform.position.y - m_offset.y);
                        m_totes.Add(new Casella(i, j, go));
                        break;
                    default:
                        break;
                }

            }
        }
    }
    public void RestartMatrix()
    {
        //Debug.Log("Child: " + transform.childCount);
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        m_empties.Clear();
        m_gujeros.Clear();
        m_pecs.Clear();
        m_camino.Clear();
        m_contigous.Clear();
        m_chuletos.Clear();
        m_pecsExtra.Clear();
        m_totes.Clear();

        Debug.Log("   ---   SPAWNER RESTART   ---   ");
        Generate();
    }
    private void extraPec()
    {
        //Debug.Log("Empties: " + m_empties.Count);
        foreach (Casella c in m_empties)
        {
            int r = Random.Range(0, m_scrip.size / 2 - 1);          // MIRAR ESTO
            //Debug.Log("r: " + r + ", Rango: "+ (m_scrip.size / 2 - 1));
            if (r == 0 && m_pecsExtra.Count < m_scrip.numPecsExtra && !onCamino(c))
            {
                m_net[c.x, c.y] = casillas.pecExtra;
                m_pecsExtra.Add(new Casella(c.x, c.y));
                //Debug.Log("EXTRA" + m_pecsExtra.Count);
            }
        }
    }
    bool onCamino(Casella c)
    {
        foreach (Casella cas in m_camino)
        {
            if (cas.x == c.x && cas.y == c.y)
            {
                //Debug.Log("OnCamino: "+cas.x+", "+cas.y); 
                return true;
            }

        }
        return false;
    }
    void followTheLeader(int c, int f)
    {
        m_netChuleto[c, f] = casillas.chuletoStart;     // Marcar la casella per la qual comenca
        m_camino.Add(new Casella(c, f));                // Agafar la coordenada d'inici perque no repetixi casella

        m_contigous = new List<Casella>();              // coordenadas adyacentes a [c,f]
        int col, fila;                                  // variables aux para que empiece a [c-1,f-1]
        m_npecs = 0;
        m_Movimentos = (m_scrip.size * m_scrip.size) / 2;
        //Debug.Log("Mov: "+m_Movimentos);

        for (int m = 0; m <= m_Movimentos; m++)          // se busca casillas hasta que no haya movimientos
        {
            for (int i = -1; i < 2; i++)
            {
                col = i + c;
                for (int j = -1; j < 2; j++)
                {
                    fila = j + f;
                    if (!isOutOfRange(col, fila)) // para el index out of bounds y la casilla [c,f] la del centro
                    {
                        if (!alreadyChoosed(col, fila))
                        {
                            //Debug.Log("Contigua: " + col + ", " + fila);
                            if (col == c || fila == f)
                                m_contigous.Add(new Casella(col, fila));
                        }
                    }
                }
            }

            // Cuando llega aqui ya deberia tener todas las coord contiguas
            int gachapon = Random.Range(0, m_contigous.Count);     // escoge una casilla de las contiguas
            m_camino.Add(m_contigous[gachapon]);
            //Debug.Log("Camino: " + m_contigous[gachapon].x + ", " + m_contigous[gachapon].y);

            //tendria que sacar las coordenadas del ultimo m_camino y separarlas en c y f           
            c = m_contigous[gachapon].x;
            f = m_contigous[gachapon].y;
            m_netChuleto[c, f] = casillas.chuleto;

            //Debug.Log(string.Format("Pecs: {0}/{1}, moviments: {2}", m_npecs, m_NumPecs, m));
            if (m == m_npecs && m != 0 && m_npecs != 0 && m_npecs < m_scrip.numPecs)                    // si los movimentos restantes = a los peces restantes; se a�ade pec si o si
            {
                m_pecs.Add(m_contigous[gachapon]);
                m_net[c, f] = casillas.pec;
                m_npecs++;
                //Debug.Log(string.Format("Iguales: m={0}, npecs={1}", m, m_npecs));
            }
            else if (m_npecs == m_scrip.numPecs)      // si todos los pecs se han distribuido que deje el bucle
            {
                m = m_Movimentos;
                //Debug.Log("NoMas");
                return;
            }
            else                                // hay pecs y mov y no son iguales
            {
                //Debug.Log("Gachapon: " + m_contigous[gachapon].x + ", " + m_contigous[gachapon].y);
                if (Random.Range(0, m_scrip.size / 2) == 0)   // Pone Pec
                {
                    //Debug.Log("pec");
                    m_pecs.Add(m_contigous[gachapon]);
                    m_net[c, f] = casillas.pec;
                    m_npecs++;
                }

            }
        }
    }

    // para el index out of bounds
    public bool isOutOfRange(int col, int fila)
    {
        if (col < 0 || col > m_scrip.size - 1 || fila < 0 || fila > m_scrip.size - 1) { return true; }
        else { return false; }
    }
    bool alreadyChoosed(int col, int fila)
    {
        //Debug.Log("Camino: ");
        foreach (Casella c in m_camino)
        {
            //Debug.Log(string.Format("{0}, {1}",c.x, c.y));
            if (c.x == fila && c.y == col)
                return true;
            if (c.x == col && c.y == fila)
                return true;
        }
        return false;
    }

    //  Me dejo esto aqui como recordatorio de lo imbecil que soy
    public List<Casella> GetCaselles()
    {
        return m_totes;
    }
}
