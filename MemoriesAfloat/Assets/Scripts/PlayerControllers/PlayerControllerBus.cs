using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControllerBus : MonoBehaviour
{
    private BusseigHandler m_busseigHandler;
    //private GameObject m_goGrab;
    [SerializeField] private PlayerScriptable m_playerScript;
    public PlayerScriptable PlayerScript { get { return m_playerScript; } }

    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;

    private Rigidbody2D m_rb;
    [SerializeField] private float m_Speed;
    private float m_sprint = 1.0f;

    [SerializeField] private GameObject m_grabCollider;
    private GameObject m_canGrab = null;
    public GameObject CanGrab { get { return m_canGrab; } set { m_canGrab = value; } }

    private List<PeixScriptable> m_capturas = new();
    public List<PeixScriptable> Capturas => m_capturas;

    private Animator m_Animator;
    private SpriteRenderer spriteRenderer;
    private AudioSource audioData;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        audioData = GetComponent<AudioSource>();

        m_busseigHandler = GameObject.Find("MiniJoc Busseig Handler").GetComponent<BusseigHandler>();

        m_rb = GetComponent<Rigidbody2D>();
        m_PlayerInputActions = new PlayerInputSystem();

        m_PlayerInputActions.MinijocBusseig.Enable();
        m_PlayerInputActions.MinijocBusseig.Moviment.performed += Moviment;
        m_PlayerInputActions.MinijocBusseig.Moviment.canceled += Stop;

        m_PlayerInputActions.MinijocBusseig.Sprint.performed += Sprint;
        m_PlayerInputActions.MinijocBusseig.Sprint.canceled += NoSprint;

        //m_PlayerInputActions.MinijocBusseig.Grab.performed += Grab;
        m_PlayerInputActions.MinijocBusseig.Grab.started += StartGrab;

        m_PlayerInputActions.MinijocBusseig.Finish.performed += Finish;
    }
    private void Moviment(InputAction.CallbackContext context)
    {
        Vector2 inputVector = m_PlayerInputActions.MinijocBusseig.Moviment.ReadValue<Vector2>();
        m_rb.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed * m_sprint;

        if (inputVector.y > 0 || inputVector.y < 0 || inputVector.x > 0)
        {
            spriteRenderer.flipX = false;
            m_grabCollider.transform.position = new Vector2(transform.position.x + 3.5f, m_grabCollider.transform.position.y);
            m_Animator.Play("Bus_Swim");
        }

        if (inputVector.x < 0)
        {
            spriteRenderer.flipX = true;
            m_grabCollider.transform.position = new Vector2(transform.position.x - 3.5f, m_grabCollider.transform.position.y);
            m_Animator.Play("Bus_Swim");
        }
    }
    private void Stop(InputAction.CallbackContext context)
    {
        m_rb.velocity = Vector3.zero;
        m_Animator.Play("PlayerBus_Idle");
    }
    private void Sprint(InputAction.CallbackContext context)
    {
        m_sprint = 1.5f;

        Vector2 inputVector = m_PlayerInputActions.MinijocBusseig.Moviment.ReadValue<Vector2>();
        m_rb.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed * m_sprint;
    }
    private void NoSprint(InputAction.CallbackContext context)
    {
        m_sprint = 1.0f;

        Vector2 inputVector = m_PlayerInputActions.MinijocBusseig.Moviment.ReadValue<Vector2>();
        m_rb.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed * m_sprint;
    }
    private void StartGrab(InputAction.CallbackContext context)
    {
        m_Animator.Play("PlayerBus_Grab");
    }

    public void Grab()
    {
        audioData.Play(0);
        if (m_canGrab != null)
        {
            m_capturas.Add(m_canGrab.GetComponent<MoluscsBehaviour>().MoluscsScript);
            //Debug.Log("Capturado: " + m_canGrab.name);
            m_busseigHandler.poof(m_canGrab);
            m_canGrab = null;
        }
    }

    private void Finish(InputAction.CallbackContext context)
    {
        m_busseigHandler.BusseigEnd();
    }
}
