using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerControllerPuerto : MonoBehaviour
{
    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;
    private Rigidbody2D m_Rigidbody;
    [SerializeField] private float m_Speed;
    [SerializeField] private GameEvent StartPreMiniJocCanya;
    private SpotPesca m_PescaSpot;
    private GameObject m_mySpot;
    public GameObject MySpot
    {
        get { return m_mySpot; }
    }

    private GameObject UI_Inventari;
    public GameObject UIInventari { get { return UI_Inventari; } }
    //private bool m_isNearVaixell;
    //public bool IsNearVaixell { set { m_isNearVaixell = value; } }


    private bool m_entrarBoat;
    public bool EntrarBoat { set { m_entrarBoat = value; } }
    private bool m_entrarBotiga;
    public bool EntrarBotiga { set { m_entrarBotiga = value; } }

    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private AudioSource audioData;

    private void Start()
    {
        UI_Inventari = GameObject.Find("UI_Inventari");
        UI_Inventari.SetActive(false);

        m_Rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        audioData = GetComponent<AudioSource>();

        m_PlayerInputActions = new PlayerInputSystem();
        m_PlayerInputActions.Port.Enable();
        m_PlayerInputActions.Port.Moviment.performed += Moviment;
        m_PlayerInputActions.Port.Moviment.canceled += Stop;
        m_PlayerInputActions.Port.Pesca.performed += Pescar;
        m_PlayerInputActions.Port.Inventari.performed += Inventari;
        m_PlayerInputActions.Port.Interactuar.performed += Interactuar;
        m_PlayerInputActions.Port.Exit.canceled -= Exit;
    }

    private void Moviment(InputAction.CallbackContext context)
    {
        Vector2 inputVector = m_PlayerInputActions.Port.Moviment.ReadValue<Vector2>();
        m_Rigidbody.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed;

        //Debug.Log("x: " + inputVector.x + " y: " + inputVector.y);
        if (inputVector.y > 0)
            animator.Play("Pescador_WalkBackwards");
        if (inputVector.y < 0)
            animator.Play("Pescador_WalkForward");

        if (inputVector.x > 0)
        {
            spriteRenderer.flipX = false;
            animator.Play("Pescador_WalkSideWays");
        }

        if (inputVector.x < 0)
        {
            spriteRenderer.flipX = true;
            animator.Play("Pescador_WalkSideWays");
        }

    }

    private void Stop(InputAction.CallbackContext context)
    {
        m_Rigidbody.velocity = Vector2.zero;
        spriteRenderer.flipX = false;
        animator.Play("Pescador_Idle");

    }

    public void SortidaPort()
    {
        m_PlayerInputActions.Port.Disable();
        GameManager.Instance.EntradaVaixell();
    }

    public void EntradaPort()
    {
        m_PlayerInputActions.Port.Enable();
    }

    private void Inventari(InputAction.CallbackContext context)
    {
        if (!UI_Inventari.activeSelf)
        {
            m_Rigidbody.velocity = Vector2.zero;
            UI_Inventari.SetActive(true);
            //UI_Inventari.GetComponent<UI_Inventari>().ShowBTNinvV = m_isNearVaixell;
            m_PlayerInputActions.Port.Moviment.performed -= Moviment;
            m_PlayerInputActions.Port.Moviment.canceled -= Stop;
            m_PlayerInputActions.Port.Pesca.performed -= Pescar;
        }
        else
        {
            UI_Inventari.SetActive(false);
            m_PlayerInputActions.Port.Moviment.performed += Moviment;
            m_PlayerInputActions.Port.Moviment.canceled += Stop;
            m_PlayerInputActions.Port.Pesca.performed += Pescar;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PlayerInputActions.Port.Pesca.performed += Pescar;
            m_PescaSpot = collision.gameObject.GetComponent<SpotPesca>();
            m_mySpot = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = null;
            m_mySpot = null;
        }
    }

    private void Pescar(InputAction.CallbackContext context)
    {
        if (m_PescaSpot != null)
        {
            audioData.Play(0);
            switch (SceneManager.GetActiveScene().name)
            {
                case "Puerto":
                    animator.Play("Pescador_CanyaSideWaysStart");
                    break;
                case "Llac":
                    animator.Play("Pescador_CanyaBackwardStart");
                    break;
            }
            m_PlayerInputActions.Port.Disable();
            StartPreMiniJocCanya.Raise();

            //m_PlayerInputActions.MinijocCanya.Enable();
        }
    }

    public void Tornada()
    {
        Debug.Log("TornadaPort");
        m_PlayerInputActions.Port.Enable();
        m_PlayerInputActions.Port.Pesca.performed -= Pescar;
        if (m_mySpot != null)
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "Puerto":
                    animator.Play("Pescador_CanyaSideWaysEnd");
                    break;
                case "Llac":
                    animator.Play("Pescador_CanyaBackwardEnd");
                    break;
            }

        }

        m_mySpot = null;
    }

    public void Exit(InputAction.CallbackContext context)
    {
        //Debug.Log("INPUT Exit");
        GameManager.Instance.UIBotiga.GetComponent<UI_TiendaController>().PlayerBackUp();
    }
    public void Interactuar(InputAction.CallbackContext context)
    {
        //Debug.Log(m_entrarBotiga);
        if (m_entrarBoat) { m_entrarBoat = false; SortidaPort(); }
        else if (m_entrarBotiga)
        {
            m_entrarBotiga = false;
            GameManager.Instance.ToggleBotiga(true);

            m_Rigidbody.velocity = Vector2.zero;
            m_PlayerInputActions.Port.Moviment.performed -= Moviment;
            m_PlayerInputActions.Port.Moviment.canceled -= Stop;
            m_PlayerInputActions.Port.Pesca.performed -= Pescar;

            m_PlayerInputActions.Port.Exit.canceled += Exit;
            //Debug.Log("InputBotiga: " + m_PlayerInputActions.Port.Exit.ToString());
        }
    }

    public void resetFromBotiga()
    {
        m_PlayerInputActions.Port.Exit.canceled -= Exit;

        m_PlayerInputActions.Port.Moviment.performed += Moviment;
        m_PlayerInputActions.Port.Moviment.canceled += Stop;
        m_PlayerInputActions.Port.Pesca.performed += Pescar;
    }

    public void StopControlls(bool state)
    {
        if (state)
        {
            m_PlayerInputActions.Port.Moviment.performed -= Moviment;
            m_PlayerInputActions.Port.Moviment.canceled -= Stop;
            m_PlayerInputActions.Port.Pesca.performed -= Pescar;
            m_PlayerInputActions.Port.Disable();
            m_Rigidbody.velocity = Vector2.zero;
        }
        else
        {
            m_PlayerInputActions.Port.Enable();
            m_PlayerInputActions.Port.Moviment.performed += Moviment;
            m_PlayerInputActions.Port.Moviment.canceled += Stop;
            m_PlayerInputActions.Port.Pesca.performed += Pescar;
        }
    }

}
