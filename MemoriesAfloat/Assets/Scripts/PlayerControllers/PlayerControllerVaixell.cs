using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.UI.Image;

public class PlayerControllerVaixell : MonoBehaviour
{
    [SerializeField]
    private PlayerScriptable m_script;
    public PlayerScriptable Script { get { return m_script; } }
    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;

    private Rigidbody2D m_rb;

    [SerializeField] private GameEvent StartPreMiniJocCanya;
    [SerializeField] private GameEvent1GameObject StartMinijocXarxa;
    [SerializeField] private GameEvent1GameObject StartMinijocBusseig;

    private SpotPesca m_PescaSpot;
    private SpotXarxa m_XarxaSpot;
    private SpotBusseig m_BussegiSpot;

    private GameObject m_mySpot;
    public GameObject MySpot { get { return m_mySpot; } }

    private Vector3 m_vector0;
    private Vector3 m_vectorf;
    private float m_distance = 0;
    private Coroutine m_corBenzina;

    [SerializeField] private GameObject UI_Inventari;

    GameObject m_invVaixell;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private AudioSource audioData;

    // + Raycast +
    //private Coroutine m_corOutOfBounds;
    //[SerializeField] float m_distStopCamara;
    //[SerializeField] ContactFilter2D m_filter;
    //private RaycastHit2D[] m_resultsRayCast = new RaycastHit2D[3];
    //private int m_numResults;
    //private bool m_topBound = false;
    //private bool m_bottomBound = false;
    //private bool m_leftBound = false;
    //private bool m_rightBound = false;

    private void Awake()
    {
        RestartComponents();
    }
    private void RestartComponents()
    {

        //UI_Inventari = GameObject.Find("UI_InventariVaixell");
        //UI_Inventari.SetActive(false);

        m_rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        audioData = GetComponent<AudioSource>();

        m_PlayerInputActions = new PlayerInputSystem();    

        m_PlayerInputActions.Vaixell.Enable();
        m_PlayerInputActions.Vaixell.Pescar.performed += Pescar;
        m_PlayerInputActions.Vaixell.Moviment.canceled += Stop;
        m_PlayerInputActions.Vaixell.Moviment.performed += Moviment;
        m_PlayerInputActions.Vaixell.Moviment.started += ManageCorutine;
        m_PlayerInputActions.Vaixell.Moviment.canceled += ManageCorutine;

        m_PlayerInputActions.Vaixell.Inventari.performed += Inventari;

        m_invVaixell = GameObject.FindObjectOfType<UI_InventariVaixell>(true).gameObject;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = collision.gameObject.GetComponent<SpotPesca>();
            m_mySpot = collision.gameObject;
        }

        if (collision.gameObject.tag == "Xarxa")
        {
            m_XarxaSpot = collision.gameObject.GetComponent<SpotXarxa>();
            m_mySpot = collision.gameObject;
        }
        if (collision.gameObject.tag == "Busseig")
        {
            m_BussegiSpot = collision.gameObject.GetComponent<SpotBusseig>();
            m_mySpot = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = null;
            m_mySpot = null;
        }

        if (collision.gameObject.tag == "Xarxa")
        {
            m_XarxaSpot = null;
            m_mySpot = null;
        }

        if (collision.gameObject.tag == "Busseig")
        {
            m_BussegiSpot = null;
            m_mySpot = null;
        }
    }

    private void Inventari(InputAction.CallbackContext context)
    {
        //Debug.Log(UI_Inventari.activeSelf);
        if (!m_invVaixell.activeSelf)
        {
            m_rb.velocity = Vector2.zero;
            m_invVaixell.SetActive(true);

            m_PlayerInputActions.Vaixell.Pescar.performed -= Pescar;
            m_PlayerInputActions.Vaixell.Moviment.canceled -= Stop;
            m_PlayerInputActions.Vaixell.Moviment.performed -= Moviment;
        }
        else
        {
            m_invVaixell.SetActive(false);

            m_PlayerInputActions.Vaixell.Pescar.performed += Pescar;
            m_PlayerInputActions.Vaixell.Moviment.canceled += Stop;
            m_PlayerInputActions.Vaixell.Moviment.performed += Moviment;
        }
    }
    private void Pescar(InputAction.CallbackContext context)
    {
        if (m_PescaSpot != null)
        {
            StopCorutineBenzina();
            animator.Play("Vaixell_StartCanya");
            audioData.Play(0);
            //StopCoroutine(m_corOutOfBounds);
            ManageInputSys(false);
            //m_PlayerInputActions.Vaixell.Disable();
            StartPreMiniJocCanya.Raise();
            //m_PlayerInputActions.MinijocCanya.Enable();

        }
        else if (m_XarxaSpot != null)
        {
            StopCorutineBenzina();
            //StopCoroutine(m_corOutOfBounds);
            animator.Play("Vaixell_XarxaStart");
            audioData.Play(0);
            ManageInputSys(false);
            StartMinijocXarxa.Raise(m_XarxaSpot.gameObject);

        }
        else if (m_BussegiSpot != null)
        {
            Debug.Log("spot");
            if (m_script.numBombonesOX > 0)
            {
                Debug.Log("oxy");
                StopCorutineBenzina();
                //StopCoroutine(m_corOutOfBounds);
                //m_PlayerInputActions.Vaixell.Disable();
                ManageInputSys(false);
                //Destroy(m_BussegiSpot.gameObject); haha no
                GameManager.Instance.LastPosMar = transform.position;
                StartMinijocBusseig.Raise(m_mySpot);
            }
        }
    }
    public void ManageInputSys(bool state)
    {

        if (state)
        {
            m_PlayerInputActions.Vaixell.Pescar.performed += Pescar;
            m_PlayerInputActions.Vaixell.Moviment.canceled += Stop;
            m_PlayerInputActions.Vaixell.Moviment.performed += Moviment;
            m_PlayerInputActions.Vaixell.Moviment.started += ManageCorutine;
            m_PlayerInputActions.Vaixell.Moviment.canceled += ManageCorutine;

            m_PlayerInputActions.Vaixell.Inventari.performed += Inventari;

            m_PlayerInputActions.Vaixell.Enable();
        }
        else
        {


            m_PlayerInputActions.Vaixell.Pescar.performed -= Pescar;
            m_PlayerInputActions.Vaixell.Moviment.canceled -= Stop;
            m_PlayerInputActions.Vaixell.Moviment.performed -= Moviment;
            m_PlayerInputActions.Vaixell.Moviment.started -= ManageCorutine;
            m_PlayerInputActions.Vaixell.Moviment.canceled -= ManageCorutine;

            m_PlayerInputActions.Vaixell.Inventari.performed -= Inventari;

            m_PlayerInputActions.Vaixell.Disable();
        }
    }
    public void Tornada(GameObject mySpot)
    {
        RestartComponents();
        m_mySpot = null;
        if (m_PescaSpot != null)
        {
            animator.Play("Vaixell_CanyaEnd");
            Destroy(m_PescaSpot.gameObject);
        }
        else if (m_XarxaSpot != null)
        {
            animator.Play("Vaixell_XarxaEnd");
            Destroy(m_XarxaSpot.gameObject);
        }

    }

    private void StopCorutineBenzina()
    {
        if (m_corBenzina == null) return;
        else
        {
            StopCoroutine(m_corBenzina);
            m_corBenzina = null;
        }
    }

    private void ManageCorutine(InputAction.CallbackContext context)
    {
        if (m_corBenzina == null)
        {
            m_corBenzina = StartCoroutine(GestioBenzina());
        }
        else
        {
            StopCoroutine(m_corBenzina);
            m_corBenzina = null;
        }

    }
    private void Moviment(InputAction.CallbackContext context)
    {
        if (m_script.benzinaActual <= 0)
        {
            //GameManager.Instance.EntradaPort();
            SortirAltaMar();
            return;
        }
        Vector2 inputVector = m_PlayerInputActions.Vaixell.Moviment.ReadValue<Vector2>();

        if (inputVector.y > 0)
        {
            spriteRenderer.flipY = false;
        }

        if (inputVector.y < 0)
        {
            spriteRenderer.flipY = true;
        }
        //m_rb.velocity = new Vector2(inputVector.x, inputVector.y) * m_script.speedVaixell;

        //m_corBenzina = StartCoroutine(GestioBenzina());
    }
    private void FixedUpdate()
    {
        Vector2 inputVector = m_PlayerInputActions.Vaixell.Moviment.ReadValue<Vector2>();
        m_rb.velocity = new Vector2(inputVector.x, inputVector.y) * m_script.speedVaixell;
    }

    private void Stop(InputAction.CallbackContext context)
    {
        //Debug.Log(string.Format("Stop: {0} / {1}", m_script.benzinaActual, m_script.benzinaMax));
        m_rb.velocity = Vector3.zero;
        //StopCoroutine(m_corBenzina);
    }
    IEnumerator GestioBenzina()
    {
        while (m_script.benzinaActual > 0)
        {
            m_vector0 = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            yield return new WaitForSeconds(0.5f);
            m_vectorf = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            m_distance += Mathf.Sqrt(Mathf.Pow((m_vectorf.x - m_vector0.x), 2) + Mathf.Pow((m_vectorf.y - m_vector0.y), 2));
            //Debug.Log("Distancia: " + m_distance);

            if (m_distance >= m_script.distanciaPerConsmir)
            {
                //Debug.Log(string.Format("Benzina: {0} / {1}", m_script.benzinaActual, m_script.benzinaMax));
                m_script.benzinaActual -= 1;
                m_distance -= m_script.distanciaPerConsmir;

                //Debug.Log("Trying: " + GameManager.Instance.UI_Manteniment);
                GameManager.Instance.UI_Manteniment.updateBenzina();

            }
        }
        SortirAltaMar();
        //GameManager.Instance.EntradaPort();

    }

    public void SortirAltaMar()
    {
        m_PlayerInputActions.Vaixell.Disable();
        GameManager.Instance.EntradaPort();
    }

    //public void EntrarAltaMar()
    //{
    //    m_PlayerInputActions.Vaixell.Enable();
    //}
}
