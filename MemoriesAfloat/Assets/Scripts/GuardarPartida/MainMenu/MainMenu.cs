using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private SaveSlotsManager saveSlotsManager;

    [SerializeField] private Button newGameBtn;
    [SerializeField] private Button loadGameBtn;

    public void OnNewGameClicked()
    {
        saveSlotsManager.ActivateMenu(false);
        DeactivateMenu();
    }

    public void OnLoadGameClicked()
    {
        saveSlotsManager.ActivateMenu(true);
        DeactivateMenu();
    }

    public void ActivateMenu()
    {
        gameObject.SetActive(true);
    }

    public void DeactivateMenu()
    {
        gameObject.SetActive(false);
    }
}
