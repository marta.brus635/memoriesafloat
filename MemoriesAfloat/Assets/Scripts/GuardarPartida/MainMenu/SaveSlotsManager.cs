using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveSlotsManager : MonoBehaviour
{
    [SerializeField] private MainMenu menu;

    private SaveSlot[] saveSlots;

    private bool isLoadingGame = false;

    private void Awake()
    {
        saveSlots = GetComponentsInChildren<SaveSlot>();
    }

    //Carregar tota la info 
    public void ActivateMenu(bool isGameData)
    {
        gameObject.SetActive(true);

        isLoadingGame = isGameData;

        Dictionary<string, GameData> profilesGameData = DataPersistanceManager.Instance.GetAllProfilesGameData();

        foreach (SaveSlot saveSlot in saveSlots)
        {
            GameData profileData = null;
            profilesGameData.TryGetValue(saveSlot.GetProfileId(), out profileData);
            //si troba es guarda contingut, sino es guarda null per gestionar el EmptyData o HasData
            saveSlot.SetData(profileData);

            if (profileData == null && isLoadingGame)
                saveSlot.SetInteractable(false);
            else
                saveSlot.SetInteractable(true);

        }
    }

    public void DeactivateMenu()
    {
        gameObject.SetActive(false);
    }

    public void OnBackClicked()
    {
        menu.ActivateMenu();
        DeactivateMenu();
    }

    public void OnSaveSlotClicked(SaveSlot saveSlot)
    {
        DataPersistanceManager.Instance.ChangeSelectedProfileId(saveSlot.GetProfileId());
        if (!isLoadingGame)
        {
            DataPersistanceManager.Instance.NewGame();
        }

        DataPersistanceManager.Instance.LoadGame();
        SceneManager.LoadScene("Puerto");

    }

    private void DisableMenuButtons()
    {
        foreach (SaveSlot saveSlot in saveSlots)
        {
            saveSlot.SetInteractable(false);
        }
    }
}
