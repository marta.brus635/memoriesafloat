using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SaveSlot : MonoBehaviour
{
    [SerializeField] private string profileId = "";

    [Header("Content")]
    [SerializeField] private GameObject noDataContent;
    [SerializeField] private GameObject hasDataContent;
    [SerializeField] private TextMeshProUGUI dinersPlayerText;
    [SerializeField] private TextMeshProUGUI algomesText;

    private Button saveSlotButton;

    private void Awake()
    {
        saveSlotButton = GetComponent<Button>();
    }

    public void SetData(GameData data)
    {
        if(data == null)
        {
            noDataContent.SetActive(true);
            hasDataContent.SetActive(false);
        }
        else
        {
            noDataContent.SetActive(false);
            hasDataContent.SetActive(true);
            dinersPlayerText.text = "Monedes: " + data.playerAttributes.diners;

        }
    }

    public string GetProfileId()
    {
        return profileId;
    }

    public void SetInteractable(bool activar)
    {
        saveSlotButton.interactable = activar;
    }
}
