using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CalendarAttributesData
{
    public Hores HoraActual;
    public int DiaActual;
    public Estacions EstacioActual;
    public Atmosferes AtmosferaActual;
    public int Hipoteca;
    public int PerlinSeed;

    //valors de NewGame
    public CalendarAttributesData()
    {
        HoraActual = Hores.mati;
        DiaActual = 1;
        EstacioActual = Estacions.primavera;
        AtmosferaActual = Atmosferes.sol;
        Hipoteca = 200;
        PerlinSeed = 0;
    }

}
