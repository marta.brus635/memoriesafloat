using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ToolsAttributesData
{
    public float percentRed;
    public int tempsBombonaAct;
    public int quantitatMilloraBenzina;
    public int quantitatMilloraSpeed;

    //Valors de NewGame
    public ToolsAttributesData()
    {
        percentRed = 0;
        tempsBombonaAct = 0;
        quantitatMilloraBenzina = 0;
        quantitatMilloraSpeed = 0;
}

}
