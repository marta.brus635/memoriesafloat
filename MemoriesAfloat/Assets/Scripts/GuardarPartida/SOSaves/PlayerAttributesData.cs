using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerAttributesData
{
    public int diners;
    public float benzinaActual;
    public float benzinaMax;
    public float velocitatVaixell;
    public int numBombonesOX;

    [System.Serializable]
    public class ItemSlot
    {
        [SerializeField] public int idObj;
        [SerializeField] public int quantitat;

        public ItemSlot(PlayerScriptable.ItemSlot obj)
        {
            idObj = obj.idObj;
            quantitat = obj.quantitat;
        }
    }

    public List<ItemSlot> inventariPlayer;


    //Valors de NewGame
    public PlayerAttributesData()
    {
        diners = 0;
        benzinaActual = 50;
        benzinaMax = 100;
        inventariPlayer = new List<ItemSlot>();
        velocitatVaixell = 4;
        numBombonesOX = 0;
    }
}
