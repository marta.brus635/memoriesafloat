using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public PlayerAttributesData playerAttributes;
    public CalendarAttributesData calendarAttributes;
    public ToolsAttributesData toolsAttributes;

    //valores que quieres que tenga el juego al empezar una partida
    public GameData()
    {
        playerAttributes = new PlayerAttributesData();
        calendarAttributes = new CalendarAttributesData();
        toolsAttributes = new ToolsAttributesData();
    }
}
