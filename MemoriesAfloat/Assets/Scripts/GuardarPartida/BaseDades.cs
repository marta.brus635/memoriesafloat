using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "BaseDades", menuName = "Objectes/BaseDadesPeixos")]
public class BaseDades : ScriptableObject
{
    public List<ObjecteScriptable> Objectes;

    public static BaseDades Instance => m_Instance;
    private static BaseDades m_Instance;

    private PeixScriptable m_peixEspecial;
    PeixScriptable[] PeixosSO;
    List<PeixScriptable> poolPeixos;
    public BaseDades()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
            throw new Exception("Nom�s pot haver una base de dades");
    }

    public ObjecteScriptable BuscarSOObjecte(int id)
    {
        return Objectes.FirstOrDefault(objecte => objecte.idObj == id);
    }

    public List<PeixScriptable> PoolPeixos()
    {
        foreach (ObjecteScriptable obj in Objectes)
        {
            if (obj is PeixScriptable)
            {
                poolPeixos.Add((PeixScriptable)obj);
            }

        }
        return poolPeixos;
    }

    public void setPriceFromSeason()
    {
        PeixosSO = FindObjectsOfType<PeixScriptable>(true);
        //obj.GetType().IsInstanceOfType(obj) == true)
        //PeixosSO = Objectes.FindAll(obj => obj is PeixScriptable peix);

        for (int i = 0; i < PeixosSO.Length; i++)
        {
            if (!GameManager.Instance.GestioDeTemps.estacioActual.Equals(PeixosSO[i].estacions))
            {
                PeixosSO[i].preuAct *= 2;
            }
            else
            {
                if ((PeixosSO[i].preuAct / PeixosSO[i].preu) > 0.5f)
                {
                    PeixosSO[i].preuAct = PeixosSO[i].preu;
                }
                else
                {
                    PeixosSO[i].PujarPreu(0.2f);
                }
            }
        }
    }

    public void setPeixEspecial()
    {
        if (m_peixEspecial != null)
        {
            m_peixEspecial.preuAct = m_peixEspecial.preu;
        }

        PeixosSO = FindObjectsOfType<PeixScriptable>(true);

        m_peixEspecial = PeixosSO[Random.Range(0, PeixosSO.Length)];
        m_peixEspecial.preuAct *= 3;
    }
}
