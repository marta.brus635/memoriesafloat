using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolsManager : MonoBehaviour, IDataPersistance
{
    [SerializeField] BusseigScriptable busScript;
    [SerializeField] XarxaScriptable xarxaScript;
    [SerializeField] BotigaRecursosScriptable botigaRecursosScript;

    public void LoadData(GameData data)
    {
        busScript.tempsBombonaAct = data.toolsAttributes.tempsBombonaAct;
        xarxaScript.percentRed = data.toolsAttributes.percentRed;
        botigaRecursosScript.QuantBenzinaExtra = data.toolsAttributes.quantitatMilloraBenzina;
        botigaRecursosScript.QuantSpeedVaixell = data.toolsAttributes.quantitatMilloraSpeed;
    }

    public void SaveData(ref GameData data)
    {
        data.toolsAttributes.tempsBombonaAct = busScript.tempsBombonaAct;
        data.toolsAttributes.percentRed = xarxaScript.percentRed;
        data.toolsAttributes.quantitatMilloraBenzina = botigaRecursosScript.QuantBenzinaExtra;
        data.toolsAttributes.quantitatMilloraSpeed = botigaRecursosScript.QuantSpeedVaixell;

    }
}
