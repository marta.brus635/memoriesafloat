using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.VisualScripting;
using UnityEngine;
using System.Linq;

public class DataPersistanceManager : MonoBehaviour
{
    [Header("File storage config")]
    [SerializeField] private string fileName;
    [SerializeField] private bool useEncryption;

    private GameData gameData;
    private List<IDataPersistance> dataPersistanceObjects;

    private FileDataHandler dataHandler;
    private bool comenzarPartida = false;

    private string selectedProfileId = "";

    public static DataPersistanceManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.Log("ya hay un DataPersistanceManager instanciado");
        }

        Instance = this;
        dataHandler = new FileDataHandler(Application.persistentDataPath, fileName, useEncryption);
        dataPersistanceObjects = FindAllDataPersistenceObjects();
        DontDestroyOnLoad(gameObject);
    }

    public void ChangeSelectedProfileId(string newProfileid)
    {
        selectedProfileId = newProfileid;
    }


    public void NewGame()
    {
        gameData = new GameData();
        comenzarPartida = true;
    }

    public void LoadGame()
    {
        if (!comenzarPartida)
        {
            gameData = dataHandler.Load(selectedProfileId);
        }

        Debug.Log("Carregant partida...");
        //Debug.Log("LoadGame():" + dataPersistanceObjects.Count);
        foreach (IDataPersistance dataPersistanceObj in dataPersistanceObjects)
        {
            //Debug.Log("Persitance: " + dataPersistanceObj);
            dataPersistanceObj.LoadData(gameData);
        }

    }

    public void SaveGame()
    {
        //Debug.Log("SaveGame():" + dataPersistanceObjects.Count);
        //recorre todos los objetos para ejecutar la funcion Save() de cada uno
        foreach (IDataPersistance dataPersistanceObj in dataPersistanceObjects)
        {
            dataPersistanceObj.SaveData(ref gameData);
        }

        dataHandler.Save(gameData, selectedProfileId);

    }

    private List<IDataPersistance> FindAllDataPersistenceObjects()
    {
        IEnumerable<IDataPersistance> dataPersistenceObjects = FindObjectsOfType<MonoBehaviour>()
            .OfType<IDataPersistance>();

        return new List<IDataPersistance>(dataPersistenceObjects);
    }

    public Dictionary<string, GameData> GetAllProfilesGameData()
    {
        return dataHandler.LoadAllProfiles();
    }
}
