using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Crafting : MonoBehaviour
{
    [SerializeField] private UI_SlotsInventari invCraft;
    private void OnEnable()
    {
        invCraft.IsCraft = true;
        invCraft.UpdateCositos();
    }
    private void OnDisable()
    {
        invCraft.IsCraft = false;
    }
}
