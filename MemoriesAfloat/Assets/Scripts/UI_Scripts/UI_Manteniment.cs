using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_Manteniment : MonoBehaviour
{
    [Header("OBJ Scriptables")]
    [SerializeField] PlayerScriptable m_playerScript;
    [SerializeField] XarxaScriptable m_xarxaScritp;
    [SerializeField] BusseigScriptable m_busseigScript;

    [Header("UI")]
    [SerializeField] TextMeshProUGUI TXT_XarxaPer;
    [SerializeField] TextMeshProUGUI TXT_NumBombona;
    [SerializeField] Image IMG_NumBombona;
    [SerializeField] TextMeshProUGUI TXT_Benzina;
    [SerializeField] Image IMG_Benzina;

    private void OnEnable()
    {
        updateBenzina();
        updateBombona();
        updateXarxa();
    }

    public void updateBenzina()
    {
        TXT_Benzina.text = m_playerScript.benzinaActual + " / " + m_playerScript.benzinaMax;
        IMG_Benzina.fillAmount = (m_playerScript.benzinaActual / m_playerScript.benzinaMax);
        //Debug.Log(string.Format("txt: {0} ({1}), fillAmount: {2}", TXT_Benzina.text, m_playerScript.benzinaActual, (m_playerScript.benzinaActual / m_playerScript.benzinaMax)));
    }
    public void updateBombona()
    {
        TXT_NumBombona.text = "" + m_playerScript.numBombonesOX;
        IMG_NumBombona.fillAmount = ((float)m_busseigScript.tempsBombonaAct / (float)m_busseigScript.tempsBombonaMax);
    }
    public void updateXarxa()
    {
        TXT_XarxaPer.text = m_xarxaScritp.percentRed + "%";
    }

}
