using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_CanviDeDia : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_dinersTotals;
    private string aux_DinersTotals;
    public string DinersTotals { set { aux_DinersTotals = value; } }

    private Coroutine cor_CanviDia;
    private bool loop = false;
    public void Start_UICanviDia()
    {
        cor_CanviDia = StartCoroutine(CanviDia());
    }

    private IEnumerator CanviDia()
    {
        loop = true;
        while (loop)
        {
            m_dinersTotals.text = aux_DinersTotals + "";

            yield return new WaitForSeconds(4f);
            loop = false;
        }
        gameObject.SetActive(false);
        GameManager.Instance.Manteniment.transform.GetChild(2).gameObject.GetComponent<UI_Diners>().updateDiners();
    }
    private void OnDisable()
    {
        StopCoroutine(cor_CanviDia);
        GameManager.Instance.PlayerPuerto.StopControlls(false);
    }
}
