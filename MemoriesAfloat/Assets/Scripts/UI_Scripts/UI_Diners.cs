using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_Diners : MonoBehaviour
{
    [Header("OBJ Scriptables")]
    [SerializeField] private PlayerScriptable playerScript;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI TXT_Diners;

    private void OnEnable()
    {
        updateDiners();
    }
    public void updateDiners()
    {
        TXT_Diners.text = "" + playerScript.diners;
    }
}
