using Microsoft.Unity.VisualStudio.Editor;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class UI_InventariVaixell : MonoBehaviour
{
    [Header("Scriptable Objects")]
    [SerializeField] private PlayerScriptable m_playerScript;

    [Header("Inventari Vaixell")]
    [SerializeField] private GameObject m_INV_Vaixell;
    public GameObject Inv_Vaixell { get { return m_INV_Vaixell; } }
    [SerializeField] private GameObject m_INV_Player;
    public GameObject Inv_Player { get { return m_INV_Player; } }

    private bool m_showBTNinvV;
    public bool ShowBTNinvV { set { m_showBTNinvV = value; } }

    private void OnEnable()
    {
        //Debug.Log(string.Format("hijo: {0}, nieto {1}", transform.GetChild(0).gameObject.name, transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.name));
        if (SceneManager.GetActiveScene().name == "AltaMar" || SceneManager.GetActiveScene().name != "AltaMar" && m_showBTNinvV)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = true;
            transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = true;
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = false;
            transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = false;
        }

    }


}
