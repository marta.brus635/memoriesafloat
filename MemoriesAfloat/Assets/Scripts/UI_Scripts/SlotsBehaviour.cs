using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotsBehaviour : MonoBehaviour, IPointerClickHandler
//, IDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    //public void OnDrag(PointerEventData eventData){Debug.Log(gameObject.name + ": Drag");}
    //public void OnPointerEnter(PointerEventData eventData) { Debug.Log(gameObject.name + ": Enter"); }
    //public void OnPointerExit(PointerEventData eventData) { Debug.Log(gameObject.name + ": Exit"); }
    private bool isSelected = false;
    private Color m_defaultColor = new Color(.94f, .96f, .74f, 1);
    private Color m_selectColor = new Color(.86f, .63f, .62f, 1);

    private UI_SlotsInventari m_papa;
    private int m_idOBJ;
    public int IdOBJ { get { return m_idOBJ; } set { m_idOBJ = value; } }

    private void Awake()
    {
        m_papa = gameObject.GetComponentInParent<UI_SlotsInventari>();
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        // carmen es idiota y marta es muy guapa, sisi si
        //Debug.Log(gameObject.name + ": Click -> " + gameObject.GetComponentInChildren<Image>(true).isActiveAndEnabled);
        //Debug.Log(gameObject.name + ": Child -> " + transform.GetChild(0).gameObject + " comp: " + gameObject.GetComponentInChildren<Image>(true));
        //Debug.Log(gameObject.name + "Estas pillando el compontente desactivado?" + transform.GetChild(0).gameObject.GetComponent<Image>());

        //Debug.Log("1 - PointerClick: " + gameObject);
        if (m_papa.CanSelect && transform.GetChild(0).gameObject.GetComponent<Image>().IsActive())
        {
            //Debug.Log("2 - PointerClick: " + gameObject);
            //if (isSelected)
            //{
            //    isSelected = false;
            //    gameObject.GetComponent<Image>().color = m_defaultColor;
            //    m_papa.RemoveItemSelected(gameObject);
            //}
            //else
            //{
            isSelected = true;
            //gameObject.GetComponent<Image>().color = m_selectColor;
            m_papa.AddItemSelected(gameObject);
            //m_papa.moveSelected();
            //}
        }
    }

    public void unSelect()
    {
        isSelected = false;
        gameObject.GetComponent<Image>().color = m_defaultColor;
    }
}
