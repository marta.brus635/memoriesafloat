using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CamaraFollowPlayer : MonoBehaviour
{
    private GameObject m_target;
    public GameObject Target
    {
        get { return m_target; }
        set { m_target = value; }
    }

    [SerializeField] private Vector3 m_offset = new Vector3(0, 0, -10);
    private Vector3 m_velocity = Vector3.zero;
    [SerializeField]
    private float m_smoothTime = 0.3f;
    private Vector3 m_playerPos = new Vector3(0, 0, 0);

    // + Limits Default +
    private float m_bottom;
    private float m_top;
    private float m_right;
    private float m_left;

    // + Limits MJBusseig +
    private bool isMJBusseig = false;
    //private float m_mjBusseigBottom = 5.0f;
    //private float m_mjBusseigTop = 25.0f;
    //private float m_mjBusseigLeft = 8.9f;
    //private float m_mjBusseigRight = 21.1f;

    // + Limits Alta Mar +
    private bool isAltaMar = false;
    private float m_altaMarBottom = 5.0f;
    private float m_altaMarTop = 25.0f;
    private float m_altaMarLeft = 8.9f;
    private float m_altaMarRight = 21.1f;

    private void Awake()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "AltaMar":
                isAltaMar = true;
                m_bottom = m_altaMarBottom;
                m_top = m_altaMarTop;
                m_left = m_altaMarLeft;
                m_right = m_altaMarRight;
                break;
            //case "Port":
            //    isPort = true;
            //    break;
            case "MJBusseig":
                isMJBusseig = true;
                //m_bottom = m_mjBusseigBottom;
                //m_top = m_mjBusseigTop;
                //m_left = m_mjBusseigLeft;
                //m_right = m_mjBusseigRight;
                break;
        }
    }

    private void LateUpdate()
    {
        if (m_target != null)
        {
            if (isAltaMar)
            {
                //  Esquinas de la pantalla
                if (m_target.transform.position.y < m_bottom && m_target.transform.position.x < m_left ||
                    m_target.transform.position.y < m_bottom && m_target.transform.position.x > m_right ||
                    m_target.transform.position.y > m_top && m_target.transform.position.x < m_left ||
                    m_target.transform.position.y > m_top && m_target.transform.position.x > m_right)
                {
                    if (m_target.transform.position.y < m_bottom && m_target.transform.position.x < m_left) { m_playerPos = new Vector3(m_left, m_bottom, 0.0f); }
                    if (m_target.transform.position.y < m_bottom && m_target.transform.position.x > m_right) { m_playerPos = new Vector3(m_right, m_bottom, 0.0f); }

                    if (m_target.transform.position.y > m_top && m_target.transform.position.x < m_left) { m_playerPos = new Vector3(m_left, m_top, 0.0f); }
                    if (m_target.transform.position.y > m_top && m_target.transform.position.x > m_right) { m_playerPos = new Vector3(m_right, m_top, 0.0f); }


                    //m_playerPos = new Vector3(transform.position.x, transform.position.y, 0.0f);
                    //Debug.Log("Esquina :" + m_playerPos);
                }
                // Limite arriba o abajo de la pantalla
                else if (m_target.transform.position.y < m_bottom || m_target.transform.position.y > m_top)
                {
                    if (m_target.transform.position.y < m_bottom) { m_playerPos = new Vector3(m_target.transform.position.x, m_bottom, 0.0f); }
                    if (m_target.transform.position.y > m_top) { m_playerPos = new Vector3(m_target.transform.position.x, m_top, 0.0f); }
                    //m_playerPos = new Vector3(m_target.transform.position.x, m_playerPos.y, 0.0f);
                    //Debug.Log("top o Botton :" + m_playerPos);
                }
                // Limite izq o der de la pantalla
                else if (m_target.transform.position.x < m_left || m_target.transform.position.x > m_right)
                {
                    if (m_target.transform.position.x < m_left) { m_playerPos = new Vector3(m_left, m_target.transform.position.y, 0.0f); }
                    if (m_target.transform.position.x > m_right) { m_playerPos = new Vector3(m_right, m_target.transform.position.y, 0.0f); }
                    //m_playerPos = new Vector3(m_playerPos.x, m_target.transform.position.y, 0.0f);
                    //Debug.Log("Left o Right :" + m_playerPos);
                }
                // Ningun limite
                else
                {
                    //Debug.Log("Nada :" + m_playerPos);
                    m_playerPos = m_target.transform.position;
                }

                transform.position = Vector3.SmoothDamp(transform.position, m_playerPos + m_offset, ref m_velocity, m_smoothTime);
            }
            if (isMJBusseig)
                transform.position = Vector3.SmoothDamp(transform.position, m_target.transform.position + m_offset, ref m_velocity, m_smoothTime);
        }
    }
}
