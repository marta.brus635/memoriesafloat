using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Calendari : MonoBehaviour, IDataPersistance
{
    [SerializeField] private GestioDeTemps m_Scriptable;

    [SerializeField] private TextMeshProUGUI m_Hora;
    [SerializeField] private TextMeshProUGUI m_Dia;
    [SerializeField] private Image m_Estacio;
    [SerializeField] private Image m_Temps;
    [SerializeField] private int PerlinSeed;

    private void Awake()
    {
        UpdateCalendar();
    }

    public void UpdateCalendar()
    {
        m_Hora.text = "Hora: " + m_Scriptable.horaActual;
        m_Dia.text = "Dia: " + m_Scriptable.diaActual;

        switch (m_Scriptable.estacioActual)
        {
            case Estacions.primavera:
                m_Estacio.sprite = m_Scriptable.SpritesEstacions[0];
                break;
            case Estacions.estiu:
                m_Estacio.sprite = m_Scriptable.SpritesEstacions[1];
                break;
            case Estacions.tardor:
                m_Estacio.sprite = m_Scriptable.SpritesEstacions[2];
                break;
            case Estacions.hivern:
                m_Estacio.sprite = m_Scriptable.SpritesEstacions[3];
                break;

        }

        switch (m_Scriptable.atmosferaActual)
        {
            case Atmosferes.sol:
                m_Temps.sprite = m_Scriptable.SpritesAtmosferes[0];
                break;
            case Atmosferes.pluja:
                m_Temps.sprite = m_Scriptable.SpritesAtmosferes[1];
                break;
            case Atmosferes.trons:
                m_Temps.sprite = m_Scriptable.SpritesAtmosferes[2];
                break;
        }
        
    }
    
    public void LoadData(GameData data)
    {
        //load the values from our game data into the SO
        m_Scriptable.diaActual = data.calendarAttributes.DiaActual;
        m_Scriptable.horaActual = data.calendarAttributes.HoraActual;
        m_Scriptable.estacioActual = data.calendarAttributes.EstacioActual;
        m_Scriptable.atmosferaActual = data.calendarAttributes.AtmosferaActual;
        m_Scriptable.PerlinSeed = data.calendarAttributes.PerlinSeed;
    }

    public void SaveData(ref GameData data)
    {
        //store the values from our SO into the game data
        data.calendarAttributes.DiaActual = m_Scriptable.diaActual;
        data.calendarAttributes.HoraActual = m_Scriptable.horaActual;
        data.calendarAttributes.EstacioActual = m_Scriptable.estacioActual;
        data.calendarAttributes.AtmosferaActual = m_Scriptable.atmosferaActual;
        data.calendarAttributes.PerlinSeed = m_Scriptable.PerlinSeed;
    }
}
