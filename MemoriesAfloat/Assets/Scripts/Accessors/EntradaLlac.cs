using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntradaLlac : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerControllerPuerto>().Actions.Port.Disable();
            GameManager.Instance.EntradaLlac();
        }
    }
}
