using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntradaBarco : MonoBehaviour
{
    private UI_InventariVaixell UI_inv;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (GameManager.Instance.PlayerScript.benzinaActual > 0)
            {
                collision.gameObject.GetComponent<PlayerControllerPuerto>().EntrarBoat = true;
            }
            else { collision.gameObject.GetComponent<PlayerControllerPuerto>().EntrarBoat = false; }
            UI_inv = collision.gameObject.GetComponent<PlayerControllerPuerto>().
                UIInventari.GetComponent<UI_InventariVaixell>();
            UI_inv.ShowBTNinvV = true;
            UI_inv.Inv_Vaixell.transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = true;
            UI_inv.Inv_Player.transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        collision.gameObject.GetComponent<PlayerControllerPuerto>().EntrarBoat = false;

        //collision.gameObject.GetComponent<PlayerControllerPuerto>().
        //    UIInventari.GetComponent<UI_InventariVaixell>().ShowBTNinvV = false;
        UI_inv.ShowBTNinvV = false;
        UI_inv.Inv_Player.transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = false;
        UI_inv.Inv_Vaixell.transform.GetChild(0).gameObject.GetComponent<UI_SlotsInventari>().CanSelect = false;

        UI_inv = null;
    }
}
