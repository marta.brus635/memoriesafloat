using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntradaPort : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.GetComponent<PlayerControllerVaixell>())
            {
                collision.GetComponent<PlayerControllerVaixell>().SortirAltaMar();
            }
            else if (collision.GetComponent<PlayerControllerPuerto>())
            {
                collision.GetComponent<PlayerControllerPuerto>().Actions.Port.Disable();
                GameManager.Instance.EntradaPort();
            }

        }
    }
}
