using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Image = UnityEngine.UI.Image;
using System.Linq;
using static UnityEditor.Progress;
using System.ComponentModel;
using Unity.VisualScripting;

public class UI_SlotsInventari : MonoBehaviour
{
    [SerializeField] private PlayerScriptable m_playerScript;
    [SerializeField] private bool m_invVaixell;

    private List<PlayerScriptable.ItemSlot> m_inv;
    private List<PlayerScriptable.ItemSlot> m_invFutur;
    private int idAUX;

    // - Borrar Inv -
    private List<GameObject> m_slotsChild = new List<GameObject>();
    private TextMeshProUGUI m_TXTquant;
    private Image m_image;

    private List<GameObject> m_goSelected = new List<GameObject>();
    private List<PlayerScriptable.ItemSlot> m_itemsSelected = new List<PlayerScriptable.ItemSlot>();
    public List<PlayerScriptable.ItemSlot> ItemsSelected { get { return m_itemsSelected; } }
    private SlotsBehaviour m_slotsBehaviour;

    [SerializeField] private bool m_canSelect = false;
    public bool CanSelect { get { return m_canSelect; } set { m_canSelect = value; } }
    [SerializeField] private bool m_isCraft = false;

    private ObjecteScriptable m_objecteAux;  // obj que recoge el BaseDades.Instance.BuscarSOObjecte(item.idObj);
    private void OnDisable()
    {
        m_canSelect = false;
    }
    private void OnEnable()
    {
        if (m_isCraft)
            m_canSelect = false;

        if (m_invVaixell)
        {
            m_inv = m_playerScript.inventariVaixell;
            m_invFutur = m_playerScript.inventariPlayer;
        }
        else
        {
            m_inv = m_playerScript.inventariPlayer;
            m_invFutur = m_playerScript.inventariVaixell;
        }


        DescolocarCosos();
        ColocarCositos();
    }
    private void DescolocarCosos()
    {
        for (int numChild = 0; numChild < transform.childCount; numChild++)
        { m_slotsChild.Add(transform.GetChild(numChild).gameObject); }

        foreach (GameObject slot in m_slotsChild)
        {
            m_TXTquant = slot.GetComponentInChildren<TextMeshProUGUI>(true);
            m_TXTquant.gameObject.SetActive(false);
            m_image = slot.transform.GetChild(0).GetComponent<Image>();
            m_image.enabled = false;
        }
    }
    private void ColocarCositos()
    {
        int s = 0;      //  no es un for porque quiero que s augmente solo si tiene quantitat > 0
        foreach (PlayerScriptable.ItemSlot item in m_inv)
        {
            if (item.quantitat > 0)
            {
                m_TXTquant = m_slotsChild[s].GetComponentInChildren<TextMeshProUGUI>(true);
                m_image = m_slotsChild[s].transform.GetChild(0).GetComponent<Image>();

                // NO CAMBIES MAS EL ORDEN DE ESTO
                if (!m_TXTquant.gameObject.activeSelf) { m_TXTquant.gameObject.SetActive(true); }
                if (!m_image.isActiveAndEnabled) { m_image.enabled = true; }

                //Debug.Log("Slot: " + m_slotsChild[s] + ", item: " + item.idObj + ", txt: " + m_TXTquant.gameObject.activeSelf + ", img: " + m_image.gameObject.activeSelf);

                m_TXTquant.text = "" + item.quantitat;
                m_image.sprite = item.objecte.sprite;

                //Debug.Log(string.Format("Item {0}: quant: {1}, image: {2}", item.idObj, m_TXTquant.text, m_image.sprite));

                // si es craft s'ha de ressaltar els que son materials per craft
                m_objecteAux = BaseDades.Instance.BuscarSOObjecte(item.idObj);

                if (m_isCraft)
                {
                    if (!m_objecteAux.isMaterial)
                    {
                        m_image.color = new Color(m_image.color.r, m_image.color.g, m_image.color.b, .5f);
                    }
                    else
                        m_image.color = new Color(m_image.color.r, m_image.color.g, m_image.color.b, 1.0f);
                }
                else
                {
                    if (!m_objecteAux.isMaterial)
                    {
                        m_image.color = new Color(m_image.color.r, m_image.color.g, m_image.color.b, 1.0f);

                    }
                }

                // A�adir el idOBJ en SlotsBehaviour
                m_slotsChild[s].GetComponent<SlotsBehaviour>().IdOBJ = item.idObj;
                s++;    // se cambia el slot
            }

        }

    }

    public bool AddItemSelected(GameObject slot)
    {
        m_goSelected.Add(slot);
        PlayerScriptable.ItemSlot item = SlotToItem(slot);
        if (item != null)
        {
            m_itemsSelected.Add(item);
            //printList(m_itemsSelected);
        }

        return false;
    }
    public bool RemoveItemSelected(GameObject slot)
    {
        m_goSelected.Remove(slot);
        PlayerScriptable.ItemSlot item = SlotToItem(slot);
        if (item != null)
        {
            m_itemsSelected.Remove(item);
            //printList(m_itemsSelected);
        }

        return false;
    }

    private PlayerScriptable.ItemSlot SlotToItem(GameObject slot)
    {
        //idAUX = slot.GetComponent<SlotsBehaviour>().IdOBJ;
        return m_inv.FirstOrDefault(i => i.idObj == slot.GetComponent<SlotsBehaviour>().IdOBJ);
    }

    private void printList(List<PlayerScriptable.ItemSlot> list)
    {
        string text = "";
        foreach (PlayerScriptable.ItemSlot item in list)
        {
            text += item.idObj + ", ";
        }
        Debug.Log(text);
    }

    public void moveSelected()
    {
        if (m_itemsSelected.Count > 0)
        {
            foreach (PlayerScriptable.ItemSlot item in m_itemsSelected)
            {
                for (int q = item.quantitat; q > 0; q--)
                {
                    m_playerScript.Emmagatzemar(item.objecte, !m_invVaixell);
                }

            }

            if (m_invVaixell)
            {
                foreach (PlayerScriptable.ItemSlot item in m_itemsSelected)
                {
                    m_playerScript.inventariVaixell.Remove(item);
                }
            }
            else
            {
                foreach (PlayerScriptable.ItemSlot item in m_itemsSelected)
                {
                    m_playerScript.inventariPlayer.Remove(item);
                }
            }

            InvUnSelect();

            m_itemsSelected.Clear();

            DescolocarCosos();
            ColocarCositos();
        }
    }

    public void InvUnSelect()
    {
        foreach (GameObject go in m_goSelected)
        {
            go.GetComponent<SlotsBehaviour>().unSelect();
        }
    }

    public void DestacarSlots(ObjecteScriptable objecte, Color color)
    {
        foreach (GameObject child in m_slotsChild)
        {
            if (child.GetComponent<SlotsBehaviour>().IdOBJ == objecte.idObj)
            {
                child.GetComponent<Image>().color = color;
            }
        }
    }
}
