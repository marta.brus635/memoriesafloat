using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControler : MonoBehaviour
{
    [SerializeField]
    private PlayerScriptable m_script;
    public PlayerScriptable Script
    {
        get { return m_script; }
    }
    PlayerInputSystem m_PlayerInputActions;
    public PlayerInputSystem Actions => m_PlayerInputActions;
    private SpotPesca m_PescaSpot;
    private SpotXarxa m_XarxaSpot;
    private Rigidbody2D m_rb;
    [SerializeField] private float m_Speed;
    [SerializeField] private GameEvent StartPreMiniJocCanya;
    [SerializeField] private GameEvent1GameObject StartMinijocXarxa;

    private GameObject m_mySpot;
    public GameObject MySpot
    {
        get { return m_mySpot; }
    }

    private Vector3 m_vector0;
    private Vector3 m_vectorf;
    private float m_distance = 0;
    private Coroutine m_corBenzina;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_PlayerInputActions = new PlayerInputSystem();

        m_PlayerInputActions.Vaixell.Enable();
        m_PlayerInputActions.Vaixell.Pescar.performed += Pescar;
        m_PlayerInputActions.Vaixell.Moviment.canceled += Stop;
        m_PlayerInputActions.Vaixell.Moviment.performed += Moviment;

        m_script.benzinaActual = m_script.benzinaMax;

    }

    private void Moviment(InputAction.CallbackContext context)
    {
        if (m_script.benzinaActual <= 0) 
        { 
            VolverAPuerto();
            return;
        }

        Vector2 inputVector = m_PlayerInputActions.Vaixell.Moviment.ReadValue<Vector2>();
        m_rb.velocity = new Vector2(inputVector.x, inputVector.y) * m_Speed;

        m_corBenzina = StartCoroutine(GestioBenzina());
    }

    private void Stop(InputAction.CallbackContext context)
    {
        m_rb.velocity = Vector3.zero;
        StopCoroutine(m_corBenzina);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = collision.gameObject.GetComponent<SpotPesca>();
            m_mySpot = collision.gameObject;
        }

        if (collision.gameObject.tag == "Xarxa")
        {
            m_XarxaSpot = collision.gameObject.GetComponent<SpotXarxa>();
            m_mySpot = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Canya")
        {
            m_PescaSpot = null;
            m_mySpot = null;
        }

        if (collision.gameObject.tag == "Xarxa")
        {
            m_XarxaSpot = null;
            m_mySpot = null;
        }
    }

    private void Pescar(InputAction.CallbackContext context)
    {
        if (m_PescaSpot != null)
        {
            m_PlayerInputActions.Vaixell.Disable();
            StartPreMiniJocCanya.Raise();
            //m_PlayerInputActions.MinijocCanya.Enable();
        }
        else if (m_XarxaSpot != null)
        {
            m_PlayerInputActions.Vaixell.Disable();
            StartMinijocXarxa.Raise(m_mySpot);
        }
    }

    public void Tornada()
    {
        m_PlayerInputActions.Vaixell.Enable();
        m_mySpot = null;
        if (m_PescaSpot != null)
        {
            Destroy(m_PescaSpot.gameObject);
        }
        else if (m_XarxaSpot != null)
        {
            Destroy(m_XarxaSpot.gameObject);
        }
    }

    IEnumerator GestioBenzina()
    {
        m_vector0 = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        yield return new WaitForSeconds(0.5f);
        m_vectorf = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        m_distance += Mathf.Sqrt(Mathf.Pow((m_vectorf.x - m_vector0.x), 2) + Mathf.Pow((m_vectorf.y - m_vector0.y), 2));
        //Debug.Log("Distancia: " + m_distance);

        if (m_distance >= m_script.distanciaPerConsmir)
        {
            m_script.benzinaActual -= 1;
            m_distance -= m_script.distanciaPerConsmir;
        }




    }

    private void VolverAPuerto()
    {
        Debug.Log("No hay gasofa");
    }
}
