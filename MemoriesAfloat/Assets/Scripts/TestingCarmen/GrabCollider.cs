using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabCollider : MonoBehaviour
{
    PlayerControllerBus m_papa;
    private void Awake()
    {
        m_papa = transform.GetComponentInParent<PlayerControllerBus>();
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.isTrigger && col.gameObject.layer == 7 && m_papa.CanGrab == null)
        {
            m_papa.CanGrab = col.gameObject;
            Debug.Log("GrabCollider: " + col.gameObject);
            m_papa.Grab();
        }

    }
}
