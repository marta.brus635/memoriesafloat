using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour, IDataPersistance
{
    /*      - Funciones del GM -
     *  Iniciar y Finalizar MiniJuegos
     *  Tocar las variables de los SObj
     *  Cambiar Escenas
     */

    private static GameManager m_Instance = null;
    public static GameManager Instance
    {
        get { return m_Instance; }
    }

    // - Xarxa -
    private GameObject m_MiniJocXarxa;
    private XarxaHandler m_XarxaHandler;

    // - Canya - 
    private GameObject m_MiniJocCanya;
    private CanyaController m_CanyaController;
    private GameObject m_PreMiniJocCanya;
    private PreCanyaController m_PreCanyaController;

    // - Busseig
    private BusseigHandler m_BusseigHandler;
    private GameObject m_mySpot;
    private GameObject[] gos;

    // - Controller -
    private PlayerControllerVaixell m_PlayerControllerVaixell;
    private PlayerControllerBus m_PlayerControllerBus;
    private PlayerControllerPuerto m_PlayerControllerPort;
    public PlayerControllerPuerto PlayerPuerto { get { return m_PlayerControllerPort; } }

    // - SObj -
    [SerializeField] private PlayerScriptable m_playerScript;
    public PlayerScriptable PlayerScript { get { return m_playerScript; } }

    [SerializeField] private BotigaRecursosScriptable m_botigaScript;
    public BotigaRecursosScriptable BotigaScript { get { return m_botigaScript; } }

    [SerializeField] private XarxaScriptable m_xarxaScript;
    public XarxaScriptable XarxaScript { get { return m_xarxaScript; } }
    [SerializeField] private BusseigScriptable m_busseigScript;
    public BusseigScriptable BusseigScript { get { return m_busseigScript; } }

    // - Aux -
    private float m_benzinaRestant;
    private int m_preuTotal;

    // - Camara - 
    private CamaraFollowPlayer m_camara;
    public CamaraFollowPlayer Camara { get { return m_camara; } }
    private GameObject m_player;        //  GO Player de l'escena actual (canvia a cada escena)

    // - Botiga -
    [SerializeField] private GameObject m_UIBotiga;
    public GameObject UIBotiga { get { return m_UIBotiga; } }

    // - Player GO -
    [SerializeField] private GameObject m_PlayerMar;
    [SerializeField] private GameObject m_PlayerPort;

    //Gestio Del Temps
    [SerializeField] private GestioDeTemps m_GestioDeTemps;
    public GestioDeTemps GestioDeTemps { get { return m_GestioDeTemps; } }
    [SerializeField] private BaseDades m_BaseDades;
    public BaseDades BaseDades { get { return m_BaseDades; } }

    // - Save Posicio Player -
    private Vector3 m_lastPosAMar = new Vector3(-999, -999, 0);
    public Vector3 LastPosMar { set { m_lastPosAMar = value; } }

    private Vector3 m_lastPosPort = new Vector3(-999, -999, 0);
    public Vector3 LastPosPort { set { m_lastPosPort = value; } }

    private Vector3 m_resetPos = new Vector3(-999, -999, 0);
    private Vector3 m_entradaLlac = new Vector3(-2.5f, 4.5f, 0);
    private Vector3 m_entradaAMar = new Vector3(2, -1.5f, 0);


    [SerializeField] private List<GameObject> spotsSpawnedAfterCleaning = new List<GameObject>();
    public List<GameObject> SpotsSpawnedAfterCleaning { get { return spotsSpawnedAfterCleaning; } }


    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else if (m_Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitValues();
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        if (scene.name == "AltaMar")
        {
            // La camara Actualiza su Target
            ActualizarCamaraTarget();

            m_PlayerControllerVaixell = GameObject.Find("VaixellV2").GetComponent<PlayerControllerVaixell>();

            // posa la Pos del player si s'ha guardat
            if (m_lastPosAMar == m_resetPos) { m_lastPosAMar = new Vector3(9, 2, 0); }
            m_player.transform.position = m_lastPosAMar;
            m_lastPosAMar = m_resetPos;

            //Xarxa
            m_MiniJocXarxa = GameObject.Find("MiniJoc Xarxa Hanlder");
            m_XarxaHandler = m_MiniJocXarxa.GetComponent<XarxaHandler>();
            m_MiniJocXarxa.SetActive(false);

            //Canya
            m_MiniJocCanya = GameObject.Find("MiniJoc Canya Handler");
            m_CanyaController = m_MiniJocCanya.GetComponentInChildren<CanyaController>();
            m_PreMiniJocCanya = GameObject.Find("PreMiniJoc Canya Handler");
            m_PreCanyaController = m_PreMiniJocCanya.GetComponent<PreCanyaController>();
            m_MiniJocCanya.SetActive(false);

        }
        if (scene.name == "Puerto")
        {
            // La camara Actualiza su Target
            ActualizarCamaraTarget();

            // Localitza la botiga i la desactiva
            //bool coge=transform.TryGetComponent<UI_TiendaController>(out UI_TiendaController aux);
            ////m_UIBotiga = GameObject.Find("UI_Botiga"); // UI_BotigaTestCarmen
            //FindObjectOfType(Type type, bool includeInactive);
            m_UIBotiga = FindObjectOfType<UI_TiendaController>(true).gameObject;

            m_PlayerControllerPort = GameObject.Find("Player").GetComponent<PlayerControllerPuerto>();

            // posa la Pos del player si s'ha guardat
            if (m_lastPosPort == m_resetPos) { m_lastPosPort = new Vector3(6.5f, 1.8f, 0); }
            //Debug.Log("Setting: " + m_lastPosPort + " as Pos");
            m_player.transform.position = m_lastPosPort;
            m_lastPosPort = m_resetPos;

            //Canya
            m_MiniJocCanya = GameObject.Find("MiniJoc Canya Handler");
            m_CanyaController = m_MiniJocCanya.GetComponentInChildren<CanyaController>();
            m_PreMiniJocCanya = GameObject.Find("PreMiniJoc Canya Handler");
            m_PreCanyaController = m_PreMiniJocCanya.GetComponent<PreCanyaController>();
            m_MiniJocCanya.SetActive(false);

        }
        if (scene.name == "MJBusseig")
        {
            m_BusseigHandler = GameObject.Find("MiniJoc Busseig Handler").GetComponent<BusseigHandler>();
            m_BusseigHandler.StartMinijoc(m_mySpot);

            // La camara Actualiza su Target
            ActualizarCamaraTarget();
            m_PlayerControllerBus = m_PlayerPort.GetComponent<PlayerControllerBus>();
        }

        if (scene.name == "Llac")
        {
            m_PlayerControllerPort = GameObject.Find("Player").GetComponent<PlayerControllerPuerto>();

            //Canya
            m_MiniJocCanya = GameObject.Find("MiniJoc Canya Handler");
            m_CanyaController = m_MiniJocCanya.GetComponentInChildren<CanyaController>();
            m_PreMiniJocCanya = GameObject.Find("PreMiniJoc Canya Handler");
            m_PreCanyaController = m_PreMiniJocCanya.GetComponent<PreCanyaController>();
            m_MiniJocCanya.SetActive(false);

        }

        //if (scene.name == "CarmenTesting")
        //{
        //    m_UIBotiga = GameObject.Find("UI_BotigaTestCarmen");
        //}

    }
    private void ActualizarCamaraTarget()
    {
        m_camara = GameObject.Find("Main Camera").GetComponent<CamaraFollowPlayer>();
        m_player = GameObject.FindGameObjectWithTag("Player");
        m_camara.Target = m_player;
    }
    private void InitValues()
    {

    }

    public void CanviarDia()
    {
        m_GestioDeTemps.CanviDia();
        m_botigaScript.GestioPujarrPreu();
        //m_UIBotiga.GetComponent<UI_TiendaController>().vendreInvVaixell();
    }


    //      +       X A R X A       +
    public void MJXarxaStart(GameObject spot)
    {
        m_player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        m_MiniJocXarxa.SetActive(true);
        RecolocarCanvasXarxa();
        m_XarxaHandler.resetHandler(spot);
    }

    public void MJXardaEnd()
    {
        //m_PlayerControllerVaixell.gameObject.SetActive(true);
        m_MiniJocXarxa.SetActive(false);
        m_PlayerControllerVaixell.Tornada(null);

        m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
    }

    private void RecolocarCanvasXarxa()
    {
        if (m_camara.Target.transform.position.x <= 9.0f && m_camara.Target.transform.position.x >= 3.0f)
        {
            m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition = new Vector3(m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition.x + 200,
                m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition.y, 0.0f);
        }
        else
        {
            m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition = new Vector3(m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition.x - 150,
                m_MiniJocXarxa.GetComponent<RectTransform>().anchoredPosition.y, 0.0f);
        }
    }

    //      -       X A R X A       -



    //      +       C A N Y A       +

    public void PreMJCanyaStart()
    {
        m_player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        m_PreCanyaController.PreMinijoc();

    }

    public void PreMJCanyaEnd()
    {
        m_PlayerControllerPort?.Tornada();
        m_PlayerControllerVaixell?.Tornada(null);
    }

    public void MJCanyaStart()
    {
        m_MiniJocCanya.SetActive(true);
        m_CanyaController.StartingGame();
    }

    public void MJCanyaEnd()
    {
        m_MiniJocCanya.SetActive(false);
        //Debug.Log("End");
        //CARMEN PORFAVOR TIENES QUE CONFIAR EN QUE ESTE IF VA BIEN
        if (m_player.TryGetComponent<PlayerControllerPuerto>(out PlayerControllerPuerto controllerPort))
        {
            controllerPort.Tornada();
        }
        if (m_player.TryGetComponent<PlayerControllerVaixell>(out PlayerControllerVaixell controllerVaixell))
        {
            controllerVaixell.Tornada(null);
        }

    }
    //      -       C A N Y A       -


    //      +       B U S S E I G       +   
    public void MJBusseigStart(GameObject spot)
    {
        m_player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        m_PlayerControllerVaixell.Actions.Vaixell.Disable();
        m_mySpot = spot;
        //Debug.Log("GM spot: "+m_mySpot);
        SceneManager.LoadScene("MJBusseig");

    }
    public void MJBusseigEnd()
    {
        SceneManager.LoadScene("AltaMar");
    }
    //      -       B U S S E I G       -


    //      +       E N T R A D A       +
    public void EntradaPort() { SceneManager.LoadScene("Puerto"); }
    public void EntradaVaixell()
    {
        m_lastPosPort = m_entradaAMar;
        SceneManager.LoadScene("AltaMar");
    }
    public void EntradaLlac()
    {
        m_lastPosPort = m_entradaLlac;
        Debug.Log("LastPos: " + m_lastPosPort);
        SceneManager.LoadScene("Llac");
    }
    public void GameOver() { Debug.Log("GAME OVER"); }
    public void ToggleBotiga(bool active)
    {
        Debug.Log("botiga: " + m_UIBotiga);
        m_UIBotiga.SetActive(active);
        // can Select a true
    }

    //      -       E N T R A D A       -


    //      +       G U A R D A R        +

    void IDataPersistance.LoadData(GameData data)
    {
        m_playerScript.inventariVaixell.Clear();
        m_playerScript.Load(data);
    }

    void IDataPersistance.SaveData(ref GameData data)
    {
        data.playerAttributes.diners = m_playerScript.diners;
        data.playerAttributes.benzinaActual = m_playerScript.benzinaActual;
        data.playerAttributes.benzinaMax = m_playerScript.benzinaMax;
        data.playerAttributes.numBombonesOX = m_playerScript.numBombonesOX;

        data.playerAttributes.velocitatVaixell = m_playerScript.speedVaixell;

        data.playerAttributes.inventariPlayer.Clear();
        foreach (PlayerScriptable.ItemSlot itemSlot in m_playerScript.inventariPlayer)
        {
            data.playerAttributes.inventariPlayer.Add(new PlayerAttributesData.ItemSlot(itemSlot));
        }
    }


    //      -       G U A R D A R        -
}
