using Microsoft.Unity.VisualStudio.Editor;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class UI_InventariVaixell : MonoBehaviour
{
    [Header("Scriptable Objects")]
    [SerializeField] private PlayerScriptable m_playerScript;

    [Header("Inventari Vaixell")]
    [SerializeField] private GameObject m_INV_Vaixell;
    [SerializeField] private GameObject m_INV_Player;

    private bool m_showBTNinvV;
    public bool ShowBTNinvV { set { m_showBTNinvV = value; } }


}
