using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISlotsEsquers : MonoBehaviour
{
    //private List<PlayerScriptable.ItemSlot> m_listEsquers;
    [SerializeField] PlayerScriptable m_playerScript;

    private List<GameObject> m_slotsChild = new List<GameObject>();
    private TextMeshProUGUI m_TXTquant;
    private Image m_image;

    //private ObjecteScriptable m_objecteAux;

    private void OnEnable()
    {
        //m_listEsquers = GameManager.Instance.PlayerScript.inventariEsquers;
        DescolocarEsquers();
        ColocarEsquers();
    }

    private void DescolocarEsquers()
    {
        for (int numChild = 0; numChild < transform.childCount; numChild++)
        { m_slotsChild.Add(transform.GetChild(numChild).gameObject); }

        foreach (GameObject slot in m_slotsChild)
        {
            m_TXTquant = slot.GetComponentInChildren<TextMeshProUGUI>(true);
            m_TXTquant.gameObject.SetActive(false);
            m_image = slot.transform.GetChild(0).GetComponent<Image>();
            m_image.enabled = false;
        }
    }
    private void ColocarEsquers()
    {
        int s = 0;      //  no es un for porque quiero que s augmente solo si tiene quantitat > 0
        foreach (PlayerScriptable.ItemSlot item in m_playerScript.inventariEsquers)
        {
            if (item.quantitat > 0)
            {
                m_TXTquant = m_slotsChild[s].GetComponentInChildren<TextMeshProUGUI>(true);
                m_image = m_slotsChild[s].transform.GetChild(0).GetComponent<Image>();

                if (!m_TXTquant.gameObject.activeSelf) { m_TXTquant.gameObject.SetActive(true); }
                if (!m_image.isActiveAndEnabled) { m_image.enabled = true; }

                m_TXTquant.text = "" + item.quantitat;
                m_image.sprite = item.objecte.sprite;

                //m_objecteAux = BaseDades.Instance.BuscarSOObjecte(item.idObj);
            }
        }
    }
}
