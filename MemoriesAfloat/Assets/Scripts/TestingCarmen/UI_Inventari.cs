using Microsoft.Unity.VisualStudio.Editor;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class UI_Inventari : MonoBehaviour
{
    [Header("TXT i IMG")]
    [SerializeField] private GameObject slotsEsquers;
    [SerializeField] private TextMeshProUGUI TXT_slotsDiners;
    [SerializeField] private TextMeshProUGUI TXT_slotsXarxa;
    [SerializeField] private TextMeshProUGUI TXT_slotsOX;
    [SerializeField] private Image IMG_slotsOX;
    [SerializeField] private Image IMG_slotsBenzina;
    [SerializeField] private TextMeshProUGUI TXT_Benzina;

    [Header("Scriptable Objects")]
    [SerializeField] private PlayerScriptable m_playerScript;
    [SerializeField] private XarxaScriptable m_xarxaScript;
    [SerializeField] private BusseigScriptable m_busseigScript;

    [Header("Inventari Vaixell")]
    [SerializeField] private GameObject m_BTN_INVVaixell;
    [SerializeField] private GameObject m_BTN_unINVVaixell;
    [SerializeField] private GameObject m_BTN_passarINVPlayer;
    [SerializeField] private GameObject m_INV_Vaixell;
    [SerializeField] private GameObject m_INV_Player;
    [SerializeField] private GameObject m_OBJ_Manteniment;
    [SerializeField] private GameObject m_OBJ_EsquersDiners;

    private bool m_showBTNinvV;
    public bool ShowBTNinvV { set { m_showBTNinvV = value; } }

    private void OnEnable()
    {
        m_INV_Player.SetActive(true);
        m_OBJ_Manteniment.SetActive(true);
        m_OBJ_EsquersDiners.SetActive(true);
        m_INV_Vaixell.SetActive(false);
        m_BTN_unINVVaixell.SetActive(false);
        m_BTN_passarINVPlayer.SetActive(false);

        //  -   Update UI Diners    -
        TXT_slotsDiners.text = "" + m_playerScript.diners;

        //  -   Update UI Xarxa    -
        TXT_slotsXarxa.text = "" + (m_xarxaScript.percentRed * 100) + " % ";

        //  -   Update UI Oxigen    -
        TXT_slotsOX.text = "" + m_playerScript.numBombonesOX;
        IMG_slotsOX.fillAmount = ((float)m_busseigScript.tempsBombonaAct / (float)m_busseigScript.tempsBombonaMax);

        //  -   Update UI Benzina    -
        IMG_slotsBenzina.fillAmount = (m_playerScript.benzinaActual / m_playerScript.benzinaMax);
        //Debug.Log("BTN: " + m_showBTNinvV);
        if (m_showBTNinvV)
        {
            m_BTN_INVVaixell.SetActive(true);

            m_INV_Player.GetComponent<UI_SlotsInventari>().CanSelect = true;
            m_INV_Vaixell.GetComponent<UI_SlotsInventari>().CanSelect = true;
        }
        else { m_BTN_INVVaixell.SetActive(false); }

        TXT_Benzina.text = m_playerScript.benzinaActual + " / " + m_playerScript.benzinaMax;
    }

    public void showInvV()
    {
        if (m_INV_Player.GetComponent<UI_SlotsInventari>().ItemsSelected.Count > 0)
        {
            m_INV_Player.GetComponent<UI_SlotsInventari>().moveSelected();
        }
        else
        {
            m_INV_Vaixell.SetActive(true);
            m_BTN_unINVVaixell.SetActive(true);
            m_BTN_passarINVPlayer.SetActive(true);
            m_BTN_INVVaixell.SetActive(false);

            m_INV_Player.SetActive(false);
            m_OBJ_Manteniment.SetActive(false);
            m_OBJ_EsquersDiners.SetActive(false);

            m_INV_Vaixell.GetComponent<UI_SlotsInventari>().CanSelect = true;
        }
    }
    public void UNshowInvV()
    {
        m_INV_Vaixell.SetActive(false);
        m_BTN_unINVVaixell.SetActive(false);
        m_BTN_passarINVPlayer.SetActive(false);
        m_BTN_INVVaixell.SetActive(true);

        m_INV_Player.SetActive(true);
        m_OBJ_Manteniment.SetActive(true);
        m_OBJ_EsquersDiners.SetActive(true);

        m_INV_Player.GetComponent<UI_SlotsInventari>().CanSelect = true;
    }
}
