using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CraftingRecepieBehaviour : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Color m_defaultColor = new Color(.94f, .96f, .74f, 1);
    private Color m_selectColor = new Color(.87f, .87f, .37f, 1);

    private Color m_slotEnough = new Color(.63f, .86f, .37f, 1);
    private Color m_slotNOTEnough = new Color(.89f, .34f, .23f, 1);

    [Header("Slots Inventari")]
    [SerializeField] private UI_SlotsInventari BG_Obj;

    [Header("S.Obj Recepta")]
    [SerializeField] private CraftScriptable m_craftScrip;

    [Header("Referencia Fills")]
    [SerializeField] private Image m_IMGResult;
    [SerializeField] private Image m_IMGMaterial;
    [SerializeField] private TextMeshProUGUI m_TXTDiners;
    [SerializeField] private TextMeshProUGUI m_TXTQuantMaterial;

    [Header("UI Crafting")]
    [SerializeField] private Image m_IMGResultCraft;
    [SerializeField] private Image m_IMGMaterialCraft;
    [SerializeField] private TextMeshProUGUI m_TXTDinersCraft;

    private int quant;
    private PlayerScriptable m_playerSript;

    private void OnEnable()
    {
        ActualitzarValors();
        m_playerSript = GameManager.Instance.PlayerScript;
    }

    private void ActualitzarValors()
    {
        m_IMGResult.sprite = m_craftScrip.sprite;
        m_IMGMaterial.sprite = m_craftScrip.material.sprite;

        m_TXTDiners.text = "" + m_craftScrip.costDiners;
        //ObjecteScriptable objAux = BaseDades.Instance.BuscarSOObjecte(m_craftScrip.material.idObj);

        quant = GameManager.Instance.PlayerScript.quantObj(m_craftScrip.material);
        if (quant != 0)
            m_TXTQuantMaterial.text = "" + m_craftScrip.quantitat + " / " + quant;
        else
            m_TXTQuantMaterial.text = "" + m_craftScrip.quantitat + " / " + 0;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Click");

        if (quant > m_craftScrip.quantitat && m_playerSript.diners >= m_craftScrip.costDiners)
        {
            m_playerSript.diners -= m_craftScrip.costDiners;
            m_playerSript.treureObj(m_craftScrip.material, m_craftScrip.quantitat);

            m_playerSript.Emmagatzemar(m_craftScrip, false);

            ActualitzarValors();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        gameObject.GetComponent<Image>().color = m_selectColor;

        if (quant < m_craftScrip.quantitat)
        {
            BG_Obj.DestacarSlots(m_craftScrip.material, m_slotNOTEnough);
        }
        else
        {
            BG_Obj.DestacarSlots(m_craftScrip.material, m_slotEnough);
        }

        m_IMGMaterialCraft.sprite = m_craftScrip.material.sprite;
        m_IMGResultCraft.sprite = m_craftScrip.sprite;
        m_TXTDinersCraft.text = "" + m_craftScrip.costDiners;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        gameObject.GetComponent<Image>().color = m_defaultColor;
        BG_Obj.DestacarSlots(m_craftScrip.material, m_defaultColor);

        m_IMGMaterialCraft.sprite = null;
        m_IMGResultCraft.sprite = null;
        m_TXTDinersCraft.text = "";
    }
}
