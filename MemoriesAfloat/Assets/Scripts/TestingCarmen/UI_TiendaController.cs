using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_TiendaController : MonoBehaviour
{

    [SerializeField] private BotigaRecursosScriptable botigaScript;
    [SerializeField] private BusseigScriptable busseigScript;
    [SerializeField] private PlayerScriptable playerScript;

    // - Canvas -
    [Header("Canvas")]
    [SerializeField] private GameObject m_BTNBenzina;
    [SerializeField] private GameObject m_BTNXarxa;
    [SerializeField] private GameObject m_BTNBombona;
    [SerializeField] private GameObject m_BTSortir;

    [SerializeField] private GameObject m_BTNCapacitatBenzina;
    [SerializeField] private GameObject m_BTNVelocitatVaixell;

    [SerializeField] private GameObject m_BTNInvPescador;
    [SerializeField] private GameObject m_UIInvPescador;

    [SerializeField] private GameObject m_UICraft;
    [SerializeField] private GameObject m_BTNCraft;

    [SerializeField] private TextMeshProUGUI m_TXTDiners;

    //[SerializeField] private GameObject m_UIInvVaixell;
    //[SerializeField] private GameObject m_BTNInvVaixell;

    private float m_benzinaRestant;
    private int m_preuTotal;

    private UI_SlotsInventari m_UIslotsInv;

    private void Awake()
    {
        m_BTNXarxa.GetComponentInChildren<TextMeshProUGUI>().text = botigaScript.preuXarxa.ToString();
        m_BTNBenzina.GetComponentInChildren<TextMeshProUGUI>().text = botigaScript.preuBenzina.ToString() + "/unitat";
        m_BTNBombona.GetComponentInChildren<TextMeshProUGUI>().text = botigaScript.preuBombona.ToString();
        m_BTNCapacitatBenzina.GetComponentInChildren<TextMeshProUGUI>().text = botigaScript.preuCapacitatBenzina.ToString();
        m_BTNVelocitatVaixell.GetComponentInChildren<TextMeshProUGUI>().text = botigaScript.preuSpeedVaixell.ToString();
    }

    void OnEnable()
    {
        m_BTNBenzina.SetActive(true);
        m_BTNXarxa.SetActive(true);
        m_BTNBombona.SetActive(true);
        m_BTNCapacitatBenzina.SetActive(true);
        m_BTNVelocitatVaixell.SetActive(true);
        m_BTSortir.SetActive(true);
        m_BTNInvPescador.SetActive(true);
        m_BTNCraft.SetActive(true);

        m_UIInvPescador.SetActive(false);
        m_UICraft.SetActive(false);

        if (botigaScript.QuantBenzinaExtra >= 10)
            m_BTNCapacitatBenzina.GetComponent<Button>().interactable = false;

        if (botigaScript.QuantSpeedVaixell >= 5)
            m_BTNVelocitatVaixell.GetComponent<Button>().interactable = false;

        if (playerScript.benzinaActual == playerScript.benzinaMax)
            m_BTNBenzina.GetComponent<Button>().interactable = false;

        CheckDiners();

        //Debug.Log(string.Format("benzina: {0}, xarxa: {1}, venta: {2}, BTN inv: {3}, UI inv: {4}", m_BTNBenzina, m_BTNXarxa, m_BTNVenta, m_BTNInvPescador, m_UIInvPescador));
    }

    public void addBombona()
    {
        if (busseigScript.tempsBombonaAct == 0 && playerScript.numBombonesOX == 0)
            busseigScript.tempsBombonaAct = busseigScript.tempsBombonaMax;
        else
            playerScript.numBombonesOX++;


        playerScript.diners -= botigaScript.preuBombona;
        CheckDiners();
    }

    public void RefillBenzina()
    {
        m_benzinaRestant = playerScript.benzinaMax - playerScript.benzinaActual;
        m_preuTotal = (int)m_benzinaRestant * botigaScript.preuBenzina;

        //  Saber si tens suficient diners per tot el que falta de benzina
        if (playerScript.diners % m_preuTotal == 0)
            botigaScript.Refil((int)m_benzinaRestant, playerScript);
        // Emplena amb els diners que tinguis
        else
            botigaScript.Refil((int)(playerScript.diners / botigaScript.preuBenzina), playerScript);

        CheckDiners();

    }


    public void showInvPescador()
    {
        m_UIInvPescador.SetActive(true);

        m_BTNBenzina.SetActive(false);
        m_BTNXarxa.SetActive(false);
        m_BTNBombona.SetActive(false);
        m_BTNCapacitatBenzina.SetActive(false);
        m_BTNVelocitatVaixell.SetActive(false);
        m_BTSortir.SetActive(false);
        m_BTNInvPescador.SetActive(false);
        m_BTNCraft.SetActive(false);

        m_UIslotsInv = m_UIInvPescador.transform.GetChild(0).GetComponent<UI_SlotsInventari>();
        m_UIslotsInv.CanSelect = true;
    }
    public void vendreInvVaixell(GestioDeTemps gestioTemps)
    {
        // Vendido general
        foreach (PlayerScriptable.ItemSlot item in playerScript.inventariVaixell)
        {
            if (item.quantitat > 0)
            {
                //GameManager.Instance.PlayerScript.diners += item.objecte.preu;
                gestioTemps.DinersPerGuanyar += item.objecte.preu;
                item.quantitat--;
                botigaScript.AffegirVenguts(item);
            }
        }
        BackToMain();
    }
    public void vendreInvPescador()
    {
        //m_MercatHandler = transform.GetChild(1).GetComponent<MercatHandler>();

        foreach (PlayerScriptable.ItemSlot item in m_UIslotsInv.ItemsSelected)
        {
            if (item.quantitat > 0)
            {
                int quant = item.quantitat;
                for (int q = quant; q > 0; q--)
                {
                    playerScript.diners += item.objecte.preu;
                    item.quantitat--;
                    botigaScript.AffegirVenguts(item);
                }
            }
        }

        m_UIslotsInv.ItemsSelected.Clear();
        m_UIslotsInv.InvUnSelect();

        BackToMain();
    }
    public void BackToMain()
    {
        m_UIslotsInv?.InvUnSelect();

        m_UIInvPescador.SetActive(false);
        m_UICraft.SetActive(false);
        //m_UIInvVaixell.SetActive(false);

        m_BTNBenzina.SetActive(true);
        m_BTNXarxa.SetActive(true);
        m_BTNBombona.SetActive(true);
        m_BTSortir.SetActive(true);
        m_BTNInvPescador.SetActive(true);
        m_BTNCapacitatBenzina.SetActive(true);
        m_BTNVelocitatVaixell.SetActive(true);
        m_BTNCraft.SetActive(true);
    }
    public void Sortir()
    {
        GameManager.Instance.ToggleBotiga(false);
        GameManager.Instance.PlayerPuerto.resetFromBotiga();
    }
    public void repararXarxa()
    {
        if (GameManager.Instance.XarxaScript.percentRed > 0.3f && playerScript.diners >= botigaScript.preuXarxa)
        {
            GameManager.Instance.XarxaScript.percentRed = 0.0f;
            playerScript.diners -= botigaScript.preuXarxa;
        }

        CheckDiners();
    }

    public void showUICraft()
    {
        m_UIInvPescador.SetActive(false);
        m_BTNBenzina.SetActive(false);
        m_BTNXarxa.SetActive(false);
        m_BTNBombona.SetActive(false);
        m_BTSortir.SetActive(false);
        m_BTNInvPescador.SetActive(false);
        m_BTNCapacitatBenzina.SetActive(false);
        m_BTNVelocitatVaixell.SetActive(false);
        m_BTNCraft.SetActive(false);

        m_UICraft.SetActive(true);

    }

    public void ComprarCapacitatBenzina()
    {
        playerScript.benzinaMax += botigaScript.BenzinaExtra;
        playerScript.diners -= botigaScript.preuCapacitatBenzina;
        botigaScript.QuantBenzinaExtra++;

        CheckDiners();
    }

    public void ComprarVelocitatVaixell()
    {
        playerScript.speedVaixell += botigaScript.SpeedVaixell;
        playerScript.diners -= botigaScript.preuSpeedVaixell;
        botigaScript.QuantSpeedVaixell++;

        if (botigaScript.QuantSpeedVaixell >= 5)
            m_BTNVelocitatVaixell.GetComponent<Button>().interactable = false;

        CheckDiners();

    }

    public void PlayerBackUp()
    {
        //Debug.Log("PlayerBackUP: " + m_UIInvPescador.activeSelf + ", " + m_UICraft.activeSelf);
        if (m_UIInvPescador.activeSelf || m_UICraft.activeSelf)
            BackToMain();
        else
            Sortir();
    }


    //desactiva o activa els botons en funcio de si tens diners o no per comprar
    private void CheckDiners()
    {
        m_TXTDiners.text = "Diners: " + playerScript.diners;
        //xarxa
        if (GameManager.Instance.XarxaScript.percentRed < 0.3f || playerScript.diners <= botigaScript.preuXarxa)
            m_BTNXarxa.GetComponent<Button>().interactable = false;
        else
            m_BTNXarxa.GetComponent<Button>().interactable = true;

        //bombona
        if (playerScript.diners < botigaScript.preuBombona)
            m_BTNBombona.GetComponent<Button>().interactable = false;
        else
            m_BTNBombona.GetComponent<Button>().interactable = true;

        //millora benzina
        if (playerScript.diners < botigaScript.preuCapacitatBenzina || botigaScript.QuantBenzinaExtra >= 10)
            m_BTNCapacitatBenzina.GetComponent<Button>().interactable = false;
        else
            m_BTNCapacitatBenzina.GetComponent<Button>().interactable = true;

        //millora vaixell
        if (playerScript.diners < botigaScript.preuSpeedVaixell || botigaScript.QuantSpeedVaixell >= 5)
            m_BTNVelocitatVaixell.GetComponent<Button>().interactable = false;
        else
            m_BTNVelocitatVaixell.GetComponent<Button>().interactable = true;

        //benzina
        if (playerScript.diners < botigaScript.preuBenzina
            || playerScript.benzinaActual == playerScript.benzinaMax)

            m_BTNBenzina.GetComponent<Button>().interactable = false;
        else
            m_BTNBenzina.GetComponent<Button>().interactable = true;

    }

}

