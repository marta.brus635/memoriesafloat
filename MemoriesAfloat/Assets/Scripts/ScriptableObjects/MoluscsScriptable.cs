using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum comportament { estatic, movil, espantadis }
[CreateAssetMenu(fileName = "MoluscsScriptable", menuName = "Scriptables/Moluscs")]
public class MoluscsScriptable : ObjecteScriptable
{
    public enum estacio { primavera, estiu, tardo, hivern }
    public enum atmosfera { sol, pluja, trons }
    public enum hores { mati, migdia, tarda, nit }


    [Header("Aparicio")]
    public List<estacio> estacions;
    public List<atmosfera> atmosferas;
    public List<hores> temps;

    [Header("Comportament Busseig")]
    public comportament comportament;
}
