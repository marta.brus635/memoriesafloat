using UnityEngine;

[CreateAssetMenu(fileName = "PruebaScriptable", menuName = "Scriptables/PruebaScriptable")]
public class PruebaScriptable : ScriptableObject
{
    public int diners;
}