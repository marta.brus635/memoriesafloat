using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CraftScriptable", menuName = "Scriptables/Craft")]
public class CraftScriptable : ObjecteScriptable
{
    [Header("Materials")]
    public ObjecteScriptable material;
    public int quantitat;
    public int costDiners;
    public int duracio;

    [Header("Efectes")]
    public bool canyaSlowFish;
    public bool canyaFancyFish;
    public bool xarxaLessHoles;
    public bool xarxaDizzyFish;
    public bool busseigChillFish;
}
