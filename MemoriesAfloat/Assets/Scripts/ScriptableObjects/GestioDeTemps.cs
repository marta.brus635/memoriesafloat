using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MatrizGeneratorV2;

public enum Estacions { primavera, estiu, tardor, hivern }
public enum Atmosferes { sol, pluja, trons }
public enum Hores { mati, migdia, tarda, nit }


[CreateAssetMenu(fileName = "GestioDeTemps", menuName = "Scriptables/Gestio de Temps")]
public class GestioDeTemps : ScriptableObject
{
    [Header("Condicions Meteorologiques")]
    [Tooltip("0:Primavera 1:Estiu 2:Tardor 3:Hivern")]
    public List<Sprite> SpritesEstacions;
    public Estacions estacioActual;
    [Tooltip("0:Sol 1:Pluja 2:Trons")]
    public List<Sprite> SpritesAtmosferes;
    public Atmosferes atmosferaActual;
    public Hores horaActual;

    [Header("Gestio dels dies")]
    [SerializeField] private GameEvent UpdateCalendar;
    public int diaActual;
    [Tooltip("Quin dia vols que sigui l'ultim de la estacio")]
    public int DiaMaximDeEstacio;
    [Tooltip("Diners que se sumaran al final del dia")]
    public int DinersPerGuanyar;

    [Header("Gestio de hipoteca")]
    public int Hipoteca;
    [Tooltip("Quan se sumara de hipoteca cada cop que no paguis")]
    public float InteresHipoteca; //cambiar por interes
    [Tooltip("Maxim de hipoteca")]
    public int MaxHipoteca;

    [Header("Gestio Temps atmosferic (Percent Acumulatiu)")]
    public int probSol;
    public int probPluja;
    public int probTrons;

    [Header("Pool de peixos")]
    [Tooltip("Tots els peixos")]
    public List<PeixScriptable> poolPeixosCompleta;
    [Tooltip("Pool de peixos de la estacio actual")]
    public List<PeixScriptable> poolPeixosActual;
    [Tooltip("Cada dia una random diferent")]
    public int PerlinSeed;

    private void Awake()
    {
        UpdatePoolActual();
    }

    public void CanviDia()
    {
        diaActual++;
        UpdatePoolActual();
        GameManager.Instance.UIBotiga.GetComponent<UI_TiendaController>().vendreInvVaixell(this);

        // Diners
        GameManager.Instance.PlayerScript.diners += DinersPerGuanyar;
        GameManager.Instance.CanviDeDia.DinersTotals = "" + DinersPerGuanyar;
        GameManager.Instance.CanviDeDia.Start_UICanviDia();
        DinersPerGuanyar = 0;
        

        GestioTempsAtmosferic();

        //PerlinSeed
        PerlinSeed = Random.Range(0, 10000);


        if (diaActual > DiaMaximDeEstacio)
        {
            diaActual = 1;

            // Canvi estacio
            switch (estacioActual)
            {
                case Estacions.primavera:
                    estacioActual = Estacions.estiu;
                    UpdatePoolActual();
                    break;
                case Estacions.estiu:
                    estacioActual = Estacions.tardor;
                    UpdatePoolActual();
                    break;
                case Estacions.tardor:
                    estacioActual = Estacions.hivern;
                    UpdatePoolActual();
                    break;
                case Estacions.hivern:
                    estacioActual = Estacions.primavera;
                    UpdatePoolActual();
                    break;
            }
            GestioHipoteca();

            BaseDades.Instance.setPriceFromSeason();
            if (diaActual % 7 == 0)
            {
                BaseDades.Instance.setPeixEspecial();
            }

        }

        UpdateCalendar.Raise();

        //m_player.GetComponent<PlayerControllerPuerto>().StopControlls(false);

    }

    private int AvisPerNoPagar = 0;

    public void GestioHipoteca()
    {
        GameManager.Instance.PlayerScript.diners -= Hipoteca;
        if (GameManager.Instance.PlayerScript.DeudaCheck())
        {
            AvisPerNoPagar++;
            Debug.Log("T'has endeutat per la hipoteca, diners actuals: " + GameManager.Instance.PlayerScript.diners);
        }
        else
        {
            AvisPerNoPagar = 0;
        }

        if (AvisPerNoPagar == 2)
        {
            Debug.Log("S'ha embaucat benzina fins cobrir el deute.");

            while (GameManager.Instance.PlayerScript.DeudaCheck())
            {
                if (GameManager.Instance.PlayerScript.benzinaActual > 0)
                {
                    GameManager.Instance.PlayerScript.benzinaActual -= 1;
                    GameManager.Instance.PlayerScript.diners += GameManager.Instance.BotigaScript.preuBenzina;
                }
                else
                {
                    Debug.Log("Benzina insuficient...");
                    GameManager.Instance.GameOver();
                    break;
                }
            }

            AvisPerNoPagar = 0;

        }

        //sumem interes despres de descomptar els diners al player
        float aux = Hipoteca * InteresHipoteca;
        Hipoteca = Mathf.RoundToInt(aux);

        if (Hipoteca >= MaxHipoteca)
            Hipoteca = MaxHipoteca;

        //Generem nova seed de perlin
        PerlinSeed = Random.Range(0, 1000);
    }

    private void GestioTempsAtmosferic()
    {
        int random = Random.Range(0, 100);
        //Debug.Log("GACHAPON: " + random);

        if (random > probTrons)
            atmosferaActual = Atmosferes.trons;
        else if (random > probPluja)
            atmosferaActual = Atmosferes.pluja;
        else
            atmosferaActual = Atmosferes.sol;

    }

    public int ContarEstacions(Estacions estPassada)
    {
        if (estacioActual == estPassada) return 0;

        int resultat = 0;
        List<Estacions> llistaEst = new List<Estacions>();
        llistaEst.Add(Estacions.hivern);
        llistaEst.Add(Estacions.tardor);
        llistaEst.Add(Estacions.estiu);
        llistaEst.Add(Estacions.primavera);

        int pos = 0;
        switch (estPassada)
        {
            case (Estacions.hivern):
                pos = 0;
                break;
            case (Estacions.tardor):
                pos = 1;
                break;
            case (Estacions.estiu):
                pos = 2;
                break;
            case (Estacions.primavera):
                pos = 3;
                break;
        }

        for (int i = pos; i < 4; i++)
        {
            if (i == 3)
            {
                if (llistaEst[3] != estacioActual)
                {
                    resultat++;
                }
                else
                {
                    return resultat;
                }
                if (i == 3) i = 0;
            }
            else
            {
                if (llistaEst[i] != estacioActual)
                {
                    resultat++;
                }
                else
                {
                    return resultat;
                }
            }
        }
        return resultat;
    }

    private void UpdatePoolActual()
    {
        poolPeixosActual.Clear();
        foreach (PeixScriptable peix in poolPeixosCompleta)
        {
            if (peix.estacions.Contains(estacioActual))
                poolPeixosActual.Add(peix);
        }
    }

    /*
    public void LoadData(GameData data)
    {
        //load the values from our game data into the SO
        diaActual = data.calendarAttributes.DiaActual;
        horaActual = data.calendarAttributes.HoraActual;
        estacioActual = data.calendarAttributes.EstacioActual;
        atmosferaActual = data.calendarAttributes.AtmosferaActual;
    }

    public void SaveData(ref GameData data)
    {
        //store the values from our SO into the game data
        data.calendarAttributes.DiaActual = diaActual;
        data.calendarAttributes.HoraActual = horaActual;
        data.calendarAttributes.EstacioActual = estacioActual;
        data.calendarAttributes.AtmosferaActual = atmosferaActual;
    }
    */
}
