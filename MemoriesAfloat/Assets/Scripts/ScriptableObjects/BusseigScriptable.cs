using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MatrizGeneratorV2;

[CreateAssetMenu(fileName = "BusseigScriptable", menuName = "Scriptables/Busseig")]
public class BusseigScriptable : ScriptableObject
{
    public int tempsBombonaAct;
    public int tempsBombonaMax;
    public List<GameObject> goMoluscs;
}
