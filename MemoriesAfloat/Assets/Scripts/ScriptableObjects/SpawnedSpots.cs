using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu(fileName = "SpawnedSpots", menuName = "Scriptables/SpawnedSpots")]
public class SpawnedSpots : ScriptableObject
{
    public List<GameObject> spawnedSpots;
}

