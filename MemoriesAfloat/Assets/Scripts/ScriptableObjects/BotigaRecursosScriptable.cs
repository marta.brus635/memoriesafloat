using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "BotigaScriptable", menuName = "Scriptables/Botiga")]
public class BotigaRecursosScriptable : ScriptableObject
{
    [Header("Preu dels Recursos")]
    public int preuBenzina;
    public int preuXarxa;
    public int preuBombona;
    public int preuCapacitatBenzina;
    public int preuSpeedVaixell;


    [Header("Quantitat dels Recuros per compra")]
    [Tooltip("Quanta benzina extra s'afegira per cada compra")]
    public int BenzinaExtra;
    [Tooltip("Quanta velocitat extra s'afegira per cada compra")]
    public float SpeedVaixell;

    [Header("Control del Mercat")]
    public List<Venguts> llistaVenguts = new List<Venguts>();
    //public List<PlayerScriptable.ItemSlot> llistaItemsVenuts = new List<PlayerScriptable.ItemSlot>();

    [Header("Quantitat de millores comprades")]
    public int QuantBenzinaExtra;
    public int QuantSpeedVaixell;

    public class Venguts
    {
        public int dia;
        public Estacions estacio;
        public int quantVenuda;
        public int idObj;

        public Venguts(int diaAcct, Estacions estacioAct, PlayerScriptable.ItemSlot item)
        {
            this.dia = diaAcct;
            this.estacio = estacioAct;
            this.idObj = item.idObj;
            this.quantVenuda = 0;
        }
    }

    public void Refil(int refil, PlayerScriptable ps)
    {
        for (int i = 0; i < refil; i++)
        {
            ps.benzinaActual++;
            ps.diners -= preuBenzina;
        }
    }

    public void AffegirVenguts(PlayerScriptable.ItemSlot item)
    {
        Venguts v = llistaVenguts.FirstOrDefault(slot => slot.idObj == item.idObj);
        if (v != null)
        {
            v.dia = GameManager.Instance.GestioDeTemps.diaActual;
            v.estacio = GameManager.Instance.GestioDeTemps.estacioActual;
            v.quantVenuda++;
            BaixarPreu(v);
        }
        else
        {
            v = new Venguts(GameManager.Instance.GestioDeTemps.diaActual, GameManager.Instance.GestioDeTemps.estacioActual, item);
            llistaVenguts.Add(v);
            v.quantVenuda++;
        }
    }
    private void BaixarPreu(Venguts v)
    {
        if (v.quantVenuda > 10)
        {
            BaseDades.Instance.BuscarSOObjecte(v.idObj).Rebaixa(0.2f);
        }
    }


    public void GestioPujarrPreu()
    {
        foreach (Venguts v in llistaVenguts)
        {
            int diesPassats = (GameManager.Instance.GestioDeTemps.diaActual - v.dia);
            int estacionsPassats = GameManager.Instance.GestioDeTemps.ContarEstacions(v.estacio);

            if (estacionsPassats > 0 || estacionsPassats == 0 && diesPassats >= 4)
            {
                BaseDades.Instance.BuscarSOObjecte(v.idObj).PujarPreu(0.2f);
            }
        }
    }
}


