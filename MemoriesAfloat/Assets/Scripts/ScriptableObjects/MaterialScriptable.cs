using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MaterialScriptable", menuName = "Scriptables/Material")]
public class MaterialScriptable : ObjecteScriptable
{
    public int quantitat;
}
