using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MatrizGeneratorV2;

[CreateAssetMenu(fileName = "XarxaScriptable", menuName = "Scriptables/Xarxa")]
public class XarxaScriptable : ScriptableObject
{

    [Header("Generar Matriz")]
    public int size;
    public GameObject prefabEmpty;  
    public GameObject prefabPec;  
    public GameObject prefabPecExtra;  
    public GameObject prefabGujero;
    public GameObject prefabChuleto;  
    public GameObject prefabChuletoStart;

    [Header("Generar el camino")]
    public int numPecs;                     // Numero de pecs que hay para el camino
    public int numPecsExtra;                // Numero de pecs para despistar
    public float percentRed;                // variable cambiante que va agumentando segun usos
    public float percentRedRota = 0.92f;    // variable que fija que la casilla sea un gujero

    [Header("Gestio dels moviments")]
    public int movMax;
    //public int movAct;


}
