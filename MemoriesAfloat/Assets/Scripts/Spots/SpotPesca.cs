using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotPesca : MonoBehaviour
{
    private SpotsManager m_papa;

    [SerializeField]
    private GestioDeTemps m_gestioDeTemps;
    [SerializeField]
    private PeixScriptable m_peixDelSpot;
    public PeixScriptable peixSpot
    {
        get { return m_peixDelSpot; }
    }
    private void Start()
    {
        m_papa = GetComponentInParent<SpotsManager>();
        int peixRandom = Random.Range(0, m_gestioDeTemps.poolPeixosActual.Count);

        m_peixDelSpot = m_gestioDeTemps.poolPeixosActual[peixRandom];
    }

    public void GetionPecesCanya(PeixScriptable peix)
    {
        if (peix != null)
            m_papa.PlayerScript.CapturaPeix(peix);

    }

    //private Animator animator;
    //private void OnEnable()
    //{
    //    animator = GetComponent<Animator>();
    //    animator.Play("CanyaSpot");
    //}
}
