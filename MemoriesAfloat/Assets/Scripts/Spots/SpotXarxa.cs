using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotXarxa : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_MJXEnd;

    private List<PeixScriptable> m_peixosCapturats;

    [SerializeField]
    private PeixScriptable peix;
    [SerializeField]
    private PeixScriptable peixExtra;


    [SerializeField]
    private GestioDeTemps m_gestioDeTemps;

    private void Start()
    {
        int peixRandom = Random.Range(0, m_gestioDeTemps.poolPeixosActual.Count);

        peix = m_gestioDeTemps.poolPeixosActual[peixRandom];
        peixExtra = m_gestioDeTemps.poolPeixosActual[peixRandom];
    }
    public void GestionPeces(int peces, int pecesExtra)
    {
        //Debug.Log("Gestionando pececitos");

        if (peces > 0)
        {
            for (int i = 1; i <= peces; i++)
            {
                GameManager.Instance.PlayerScript.CapturaPeix(peix);
            }
        }
        if (pecesExtra > 0)
        {
            for (int i = 1; i <= pecesExtra; i++)
            {
                GameManager.Instance.PlayerScript.CapturaPeix(peix);
            }
        }

        m_MJXEnd.Raise();
    }
}
